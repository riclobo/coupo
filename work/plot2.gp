#
# Axis fomatting
#
set xdata time
set timefmt x "%d/%m/%Y"
set format x "%y"

set xlabel "Year"
set xtics scale 0

set ydata
set format y "%g"

set ylabel "Distance"

#
# Style
#
set boxwidth oneyear
set style fill solid 0.75
#
# Plot 
#
set xrange["01/04/1998":"01/09/2014"]
set yrange[0:]

#
# Plot definitions
#
p2_data="'distyear.dat' using 1:2"
p2_type="with boxes"
p2_style="lc rgb\"#FF4500\" notitle"

#
# Plot
#
plot @p2_data @p2_type @p2_style
