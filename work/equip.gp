set datafile separator "\t"
#
# Axis fomatting
#
set xdata 

set xlabel "Equipment"
set xtics scale 0
set xtics rotate

set ydata
set format y "%g"

set ylabel "Distance (km)"

#
# Style
#
set style fill solid 0.5

#
# Plot 
#
#set xrange["01/04/1998":"01/09/2014"]
set yrange[0:]

#
# Plot definitions
#
p2_data="'equip.dat' using 2:xtic(1)"
p2_type="with boxes"
p2_style="lc rgb\"#FF1493\" notitle"

#
# Plot
#
plot @p2_data @p2_type @p2_style

