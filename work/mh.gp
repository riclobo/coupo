#
# Axis fomatting
#
#set xdata time
#set timefmt x "%Y"
#set format x "%Y"
set xlabel "Year"
#set xtics scale 0

set ydata
set format y "%g"

set ylabel "Distance"

#
# Style
#
#dx=5.
#n=2
#total_box_width_relative=0.75
#gap_width_relative=0.1
#d_width=(gap_width_relative+total_box_width_relative)*dx/2.
#set boxwidth total_box_width_relative/n relative

n=4		# number of items
bw=0.9		# relative box width
gap=0.15		# interbar gap
dyr=bw/(2*n)	# year shift for the bars	
set boxwidth (bw-gap)/n relative
dd=-dyr;


set style fill solid 0.75
#
# Plot 
#
set xrange[2003:2015]
set yrange[0:]
set grid
#
# Plot definitions
#
p2_data="'mh.dat' using ($1-3*dyr):2"
p2_type="with boxes"
p2_style="lc rgb\"#FF4500\" title \"All\""

p3_data="'mh.dat' using ($1+dd):3"
p3_type="with boxes"
p3_style="lc rgb\"#FF0000\" title \"Run\""

p4_data="'mh.dat' using ($1+dyr):4"
p4_type="with boxes"
p4_style="lc rgb\"#00FF00\" title \"Cycle\""

p5_data="'mh.dat' using ($1+3*dyr):5"
p5_type="with boxes"
p5_style="lc rgb\"#0000FF\" title \"Swim\""


#
# Plot
#
plot @p2_data @p2_type @p2_style, \
     @p3_data @p3_type @p3_style, \
     @p4_data @p4_type @p4_style, \
     @p5_data @p5_type @p5_style

reset

