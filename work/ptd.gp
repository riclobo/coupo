set datafile separator "\t"

#
# Axis fomatting
#
set xdata 
set xlabel "Distance (km)"
set format x "%g"

set timefmt y "%H:%M:%S"
set ydata time
set ylabel "Time (hours)"
set format y "%H:%M"

#
# Style 
#
set style line 1006 lc rgb"#FF4500" lt 1 lw 1 pt 7 ps 0.5
set style line 1007 lc rgb"#1E90FF" lt 1 lw 4 pt 7 ps 0.05

set style line 2001 lt 0 lc rgb"#483D8B" lw 2
set style line 2002 lt 0 lc rgb"#00BFFF"


#
# Plot 
#
set xrange[0:]
set yrange["0:00":]
set grid xtics ls 2001
set grid ytics mytics ls 2001, ls 2002

set key top left

#
#
#

f(x) = a * x
fit f(x) 'td.dat' using 1:2 via a

#
# Plot definitions
#
pl1_a1="'td.dat' using 1:2"
pl1_b1="with points ls 1007 title 'Events'"
pl1_a2="363 * x"
pl1_b2="with lines ls 1006 title 'Fit'"

#
# Plot
#
plot @pl1_a1 @pl1_b1, \
     @pl1_a2 @pl1_b2
