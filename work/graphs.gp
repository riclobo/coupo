#
# General settings
#
unset multiplot
set macros

#
# Definitions
#
oneyear = 60*60*24*365

#set term wxt 0 enhanced size 500, 750 font ",12"
#set multiplot layout 2,1
#set lmargin 10
#load "plot1.gp"

#set term wxt 1 enhanced size 750, 500 font ",12"
#load "plot2.gp"

#set term wxt 0 enhanced size 750, 500 font ",12"
#load "plot3.gp"

set term wxt 0 enhanced size 500,750 font ",12"
load "equip.gp"


#unset multiplot
