set datafile separator "\t"

#
# Axis fomatting
#
set timefmt x "%d/%m/%Y"
set xdata time
set xlabel "Month"
set format x "%b"

set timefmt y "%M:%S"
set ydata time
set ylabel "Pace (min/km)"
set format y "%M:%S"

#
# Style 
#
set style line 1000 lc rgb"black"   lt 1 lw 1 pt 7 ps 0.5
set style line 1001 lc rgb"#DDA0DD" lt 1 lw 1 pt 7 ps 0.5
set style line 1002 lc rgb"#9FAFDF" lt 1 lw 1 pt 7 ps 0.5
set style line 1003 lc rgb"#F4A460" lt 1 lw 1 pt 7 ps 0.5
set style line 1004 lc rgb"#66CDAA" lt 1 lw 1 pt 7 ps 0.5
set style line 1005 lc rgb"#FFD700" lt 1 lw 4 pt 7 ps 1
set style line 1006 lc rgb"#FF4500" lt 1 lw 4 pt 7 ps 1
set style line 1007 lc rgb"#1E90FF" lt 1 lw 4 pt 7 ps 1

set style line 2001 lt 0 lc rgb"#483D8B" lw 2
set style line 2002 lt 0 lc rgb"#00BFFF"


#
# Plot 
#
set xrange["01/01/2000":"31/12/2000"]
set yrange["5:00":"8:00"]
set key maxrows 4
set grid xtics ls 2001
set grid ytics mytics ls 2001, ls 2002

#
# Plot definitions
#
pl1_a1="'comp.dat' using 1:2"
pl1_b1="with linespoints ls 1001 title '2008'"
pl1_a2="'comp.dat' using 3:4"
pl1_b2="with linespoints ls 1002 title '2009'"
pl1_a3="'comp.dat' using 5:6"
pl1_b3="with linespoints ls 1003 title '2010'"
pl1_a4="'comp.dat' using 7:8"
pl1_b4="with linespoints ls 1004 title '2011'"
pl1_a5="'comp.dat' using 9:10"
pl1_b5="with linespoints ls 1005 title '2012'"
pl1_a6="'comp.dat' using 11:12"
pl1_b6="with linespoints ls 1006 title '2013'"
pl1_a7="'comp.dat' using 13:14"
pl1_b7="with linespoints ls 1007 title '2014'"

#
# Plot
#
plot @pl1_a1 @pl1_b1, \
     @pl1_a2 @pl1_b2, \
     @pl1_a3 @pl1_b3, \
     @pl1_a4 @pl1_b4, \
     @pl1_a5 @pl1_b5, \
     @pl1_a6 @pl1_b6, \
     @pl1_a7 @pl1_b7,  
