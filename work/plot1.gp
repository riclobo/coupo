#
# Axis fomatting
#
set timefmt x "%d/%m/%Y"
set xdata time
set xlabel "Year"
set format x "%Y"

set timefmt y "%H:%M:%S"
set ydata time
set ylabel "Time"
set format y "%H:%M"

#
# Plot 
#
#set xrange["01/04/1998":"01/09/2014"]
set yrange[0:]

#
# Plot definitions
#
pl1_a="'distyear.dat' using 1:3"
pl1_b="with linespoints"

#
# Plot
#
plot @pl1_a @pl1_b
