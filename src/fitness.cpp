//----------------------------------------------------------------------
// Class definition for an ensemble of exercises
// (c) R.P.S.M. Lobo -- rpsml@yahoo.com
//----------------------------------------------------------------------
#include <fstream>
#include <sstream>
#include <numeric>
#include <algorithm>
#include <iomanip>
//----------------------------------------------------------------------
#include "fitness.h"
#include "datetime.h"
//----------------------------------------------------------------------
// Add a new item to the activities vector
//----------------------------------------------------------------------
void fitness::Add(time_t TimeStamp,
                  unsigned Act,
                  const std::string &Equip,
                  const std::string &Location,
                  const std::string &Note,
                  bool Competition,
                  double Dist, unsigned Duration,
                  unsigned HRMAX, unsigned HRAVE,
                  double Weight)
{
  acts.push_back( exercise(TimeStamp, Act, Equip, Location, Note, Competition,
                           Dist, Duration, HRMAX, HRAVE, Weight) );
}
//----------------------------------------------------------------------
// Add a new item to the activities vector
//----------------------------------------------------------------------
void fitness::Add(const exercise &activity)
{
  acts.push_back(activity);
}
//----------------------------------------------------------------------
// Remove all items in range
//----------------------------------------------------------------------
bool fitness::Remove(size_t index, size_t count)
{
  if (count == 0)     // Must remove at least one item
    return false;

  size_t i2 = index + count - 1;
  if ( i2 >= acts.size() || index >= acts.size() )   // Out of bounds
    return false;

  if (count > 1)          // Erase range
    acts.erase( acts.begin() + index, acts.begin() + i2 );
  else                    // Erase item
    acts.erase( acts.begin() + index );

  return true;
}
//----------------------------------------------------------------------
// Create sorting vectors
//----------------------------------------------------------------------
void fitness::Sort()
{
  // Redimentions index vectors to have them with the same size as acts
  RunSpeed.resize(acts.size());
  RunDistance.resize(acts.size());

  // Fills vectors with sequential indexes
  std::iota(RunSpeed.begin(), RunSpeed.end(), 0);
  std::iota(RunDistance.begin(), RunDistance.end(), 0);

  // Sorts indexes vectors according to speed and distance
  std::stable_sort( RunSpeed.begin(), RunSpeed.end(),
             [&](size_t left, size_t right)
             { return acts[left].Speed() > acts[right].Speed(); } );

  std::stable_sort( RunDistance.begin(), RunDistance.end(),
             [&](size_t left, size_t right)
             { return acts[left].GetDistance() > acts[right].GetDistance();} );

  // Copies sorted index vectors to Swim and Cycle versions
  SwimSpeed.assign(RunSpeed.begin(),RunSpeed.end());
  SwimDistance.assign(RunDistance.begin(),RunDistance.end());

  CycleSpeed.assign(RunSpeed.begin(),RunSpeed.end());
  CycleDistance.assign(RunDistance.begin(),RunDistance.end());

  // Goes through each vector and flags elements which are not related to the specific exercise
  for (auto &i : RunSpeed)
    if ( acts[i].GetExercise() != al::run )
      i = MAX_SIZE_T;

  for (auto &i : RunDistance)
    if ( acts[i].GetExercise() != al::run )
      i = MAX_SIZE_T;

  for (auto &i : SwimSpeed)
    if ( acts[i].GetExercise() != al::swim )
      i = MAX_SIZE_T;

  for (auto &i : SwimDistance)
    if ( acts[i].GetExercise() != al::swim )
      i = MAX_SIZE_T;

  for (auto &i : CycleSpeed)
    if ( acts[i].GetExercise() != al::cycle )
      i = MAX_SIZE_T;

  for (auto &i : CycleDistance)
    if ( acts[i].GetExercise() != al::cycle )
      i = MAX_SIZE_T;

  // Erases flaged elements
  size_t maxsize = MAX_SIZE_T;
  RunSpeed.erase( std::remove(RunSpeed.begin(), RunSpeed.end(), maxsize), RunSpeed.end() );
  RunDistance.erase( std::remove(RunDistance.begin(), RunDistance.end(), maxsize), RunDistance.end() );
  SwimSpeed.erase( std::remove(SwimSpeed.begin(), SwimSpeed.end(), maxsize), SwimSpeed.end() );
  SwimDistance.erase( std::remove(SwimDistance.begin(), SwimDistance.end(), maxsize), SwimDistance.end() );
  CycleSpeed.erase( std::remove(CycleSpeed.begin(), CycleSpeed.end(), maxsize), CycleSpeed.end() );
  CycleDistance.erase( std::remove(CycleDistance.begin(), CycleDistance.end(), maxsize), CycleDistance.end() );
}
//----------------------------------------------------------------------
// Last 'n' exercises
//----------------------------------------------------------------------
const std::string fitness::Last(unsigned exercise, size_t n) const
{
  std::stringstream rv {""};

  size_t k = 1;
  for (auto e = acts.rbegin(); e != acts.rend(); e++)
  {
    if (e->GetExercise() == exercise)
    {
      rv << std::setw(2) << k << ". " << e->OneLiner() << std::endl;
      if (k == n) break;
      k++;
    }
  }

  return rv.str();
}
//----------------------------------------------------------------------
// Fastest 'n' exercises
//----------------------------------------------------------------------
const std::string fitness::Fastest(unsigned exercise, size_t n) const
{
  std::stringstream rv {""};

  const std::vector<size_t> &v = exercise == al::run ? RunSpeed : (exercise == al::cycle ? CycleSpeed : SwimSpeed);
  size_t N = (v.size() > n) ? n : v.size();

  for (size_t i = 0; i < N; i++)
    rv << std::setw(2) << i + 1 << ". " << acts[v[i]].OneLiner() << std::endl;

  return rv.str();
}
//----------------------------------------------------------------------
// Slowest 'n' exercises
//----------------------------------------------------------------------
const std::string fitness::Slowest(unsigned exercise, size_t n) const
{
  std::stringstream rv {""};

  const std::vector<size_t> &v = exercise == al::run ? RunSpeed : (exercise == al::cycle ? CycleSpeed : SwimSpeed);
  size_t N = (v.size() > n) ? n : v.size();

  for (size_t i = v.size() - N; i < v.size(); i++)
    rv << acts[v[i]].OneLiner() << std::endl;

  return rv.str();
}
//----------------------------------------------------------------------
// Longest 'n' exercises
//----------------------------------------------------------------------
const std::string fitness::Longest(unsigned exercise, size_t n) const
{
  std::stringstream rv {""};

  const std::vector<size_t> &v = exercise == al::run ? RunDistance : (exercise == al::cycle ? CycleDistance : SwimDistance);
  size_t N = (v.size() > n) ? n : v.size();

  for (size_t i = 0; i < N; i++)
    rv << std::setw(2) << i + 1 << ". " << acts[v[i]].OneLiner() << std::endl;

  return rv.str();
}
//----------------------------------------------------------------------
// Shortest 'n' exercises
//----------------------------------------------------------------------
const std::string fitness::Shortest(unsigned exercise, size_t n) const
{
  std::stringstream rv {""};

  const std::vector<size_t> &v = exercise == al::run ? RunDistance : (exercise == al::cycle ? CycleDistance : SwimDistance);
  size_t N = (v.size() > n) ? n : v.size();

  for (size_t i = v.size() - N; i < v.size(); i++)
    rv << acts[v[i]].OneLiner() << std::endl;

  return rv.str();
}
//----------------------------------------------------------------------
// Customizes which header titles are written to the output stream
//----------------------------------------------------------------------
void fitness::CustomHeader(std::ostream &os, unsigned Options)
{
  std::string Header = "";

  if (Options & xpDate)       Header += "Date\t";
  if (Options & xpDist)       Header += "Distance\t";
  if (Options & xpDuration)   Header += "Duration\t";
  if (Options & xpPace)       Header += "Pace\t";
  if (Options & xpSpeed)      Header += "Speed\t";
  if (Options & xpEquipment)  Header += "Equipement\t";
  if (Options & xpCompet)     Header += "Competition\t";
  if (Options & xpHRMAX)      Header += "Max HR\t";
  if (Options & xpHRAVE)      Header += "Ave HR\t";
  if (Options & xpHRKm)       Header += "Beats/km\t";
  if (Options & xpWeight)     Header += "Weight\t";
  if (Options & xpAct)        Header += "Activity\t";
  if (Options & xpLocation)   Header += "Location\t";
  if (Options & xpNote)       Header += "Notes\t";

  if (!Header.empty()) Header.pop_back();   // Erases trailing \t

  os << Header << std::endl;    // Sends string to stream
}
//----------------------------------------------------------------------
// Customizes which items are written to the output stream
//----------------------------------------------------------------------
void fitness::CustomStream(std::ostream &os,
                           unsigned Options, const exercise &e)
{
  std::stringstream ss;

  if (Options & xpDate)
  {
    if (Options & xpUnixDate)
      ss << e.GetTimeStamp() << '\t';
    else
      ss << lobo::DDMMYYYY(e.GetTimeStamp()) << '\t';
  }

  if (Options & xpDist)
  {
    if (Options & xpDist_M)
      ss << int(e.GetDistance() * 1000.0) << '\t';
    else
      ss << e.GetDistance() << '\t';
  }

  if (Options & xpDuration)
  {
    if (Options & xpDuration_M)
      ss << lobo::mmmss(e.GetDuration()) << '\t';
    else
      ss << lobo::hhmmss(e.GetDuration()) << '\t';
  }

  if (Options & xpPace)       ss << lobo::mmmssxxx(e.Pace()) << '\t';
  if (Options & xpSpeed)      ss << e.Speed() << '\t';
  if (Options & xpEquipment)  ss << e.GetEquipment() << '\t';

  if (Options & xpHRMAX)  ss << e.GetMaxHR() << '\t';
  if (Options & xpHRAVE)  ss << e.GetAveHR() << '\t';
  if (Options & xpHRKm)   ss << e.BeatsKm() << '\t';

  if (Options & xpWeight)
  {
    if (Options & xpWeight_G)
      ss << int(e.GetWeight() * 1000.0)  << '\t';
    else
      ss << e.GetWeight() << '\t';
  }

  if (Options & xpAct)
  {
    if (Options & xpActCode)
      ss << e.GetExercise() << '\t';
    else
      ss << exercise::ExeName(e.GetExercise()) << '\t';
  }

  if (Options & xpLocation) ss << e.GetLocation() << '\t';
  if (Options & xpCompet)   ss << e.GetCompetition() << '\t';
  if (Options & xpNote)     ss << e.GetNote() << '\t';

  std::string s = ss.str();       // Convert string stream to string
  if (!s.empty()) s.pop_back();   // Erase trailing \t
  os << s  << std::endl;          // Write string to stream
}
//----------------------------------------------------------------------
// Exports data
//----------------------------------------------------------------------
bool fitness::Export(const std::string &FileName, unsigned Options,
                     size_t index, size_t count)
{
  if (count == 0)     // Must export at least one item
    return false;

  if ( index >= acts.size() )   // Out of bounds
    return false;

  size_t i2;
  if (count == MAX_SIZE_T)
    i2 =  acts.size();
  else
    i2 = index + count;

  if (i2 > acts.size())
    i2 = acts.size();

  CustomHeader(std::cout, Options);

  // Iterates through range
  for (auto activity = begin(acts) + index;
       activity != begin(acts) + i2;
       activity++)
  {
    CustomStream(std::cout, Options, *activity);
  }

  return true;
}
//----------------------------------------------------------------------
// First day an equipment was used
//----------------------------------------------------------------------
time_t fitness::FindFirstEquip(const std::string &eq) const
{
  auto it = acts.begin();
  while ( it->GetEquipment() != eq && it != acts.end()) it++;
  return it->GetTimeStamp();
}
//----------------------------------------------------------------------
// Last day an equipment was used
//----------------------------------------------------------------------
time_t fitness::FindLastEquip(const std::string &eq) const
{
  auto it = acts.rbegin();
  while ( it->GetEquipment() != eq && it != acts.rend() ) it++;   // Note ++ instead of --: Reverse iterator
  return it->GetTimeStamp();
}
//----------------------------------------------------------------------
// Read data from file
//----------------------------------------------------------------------
bool fitness::ReadFile(const std::string &FileName)
{
  std::ifstream FileIn(FileName);
  if (FileIn.fail()) return false;

  acts.clear();

  while (!FileIn.eof())                 // Goes through the whole file
  {
    std::string s;
    std::getline(FileIn, s);          // Reads one line at a time
    if (!s.empty() && (s[0] != '#'))  // Avoids comments & empty lines
    {
      std::stringstream ss;
      exercise act;

      ss.str(s);        // Puts the string in a stream
      ss >> act;        // Reads the stream
      Add(act);         // Adds the action to the list
    }
  }

  return true;
}
//----------------------------------------------------------------------
// Write data to file
//----------------------------------------------------------------------
bool fitness::WriteFile(const std::string &FileName)
{
  std::ofstream FileOut(FileName);
  if (FileOut.fail()) return false;

  FileOut << "# " << exercise::Header() << std::endl;   // Writes header
  FileOut << *this;                              // Writes exercise list

  return true;
}
//----------------------------------------------------------------------
// friend -- Stream out
//----------------------------------------------------------------------
std::ostream &operator<<(std::ostream &os, const fitness &f)
{
  // Iterates through all elements and sends each to the stream
  for (auto &activity : f.acts)   // auto --> const exercise
    os << activity;

  return os;
}
