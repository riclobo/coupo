//----------------------------------------------------------------------
// Functions for trimming, reducing and similar string operations
//----------------------------------------------------------------------
#ifndef TRIM_H
#define TRIM_H
//----------------------------------------------------------------------
#include <string>
//----------------------------------------------------------------------
namespace lobo
{
  std::string ltrim(const std::string &s);
  std::string rtrim(const std::string &s);
  std::string trim(const std::string &s);
  std::string reduce(const std::string &s,
                     const std::string &filler = " ",
                     const std::string &spacer = " \t");
  std::string smart_space_eraser(const std::string &s,
                                 char quote = '\"',
                                 bool ToLowerCase = false);
}
#endif // TRIM_H
