//----------------------------------------------------------------------
// Date and time conversion functions
// (c) R.P.S.M. Lobo -- rpsml@yahoo.com
//----------------------------------------------------------------------
#include "datetime.h"
#include <sstream>
#include <iomanip>
//----------------------------------------------------------------------
// tm struc contents:
//   tm_sec    int  seconds after the minute   0-60*
//   tm_min    int  minutes after the hour     0-59
//   tm_hour   int  hours since midnight       0-23
//   tm_mday   int  day of the month           1-31
//   tm_mon    int  months since January       0-11
//   tm_year   int  years since 1900
//   tm_wday   int  days since Sunday          0-6
//   tm_yday   int  days since January 1       0-365
//   tm_isdst  int  Daylight Saving Time flag
//----------------------------------------------------------------------
// Create a time_t structure from date and time
//----------------------------------------------------------------------
time_t lobo::SetDate(int day, int month, int year,
							       int hour, int min, int sec)
{
  tm timeinfo;          // See tm contents in the top of this file

  timeinfo.tm_mday = day;
  timeinfo.tm_mon = month - 1;
  timeinfo.tm_year = year - 1900;
  timeinfo.tm_hour = hour;
  timeinfo.tm_min = min;
  timeinfo.tm_sec = sec;

  return mktime(&timeinfo);
}
//----------------------------------------------------------------------
// Create a time_t structure from date and time "dd/mm/yyyy"
//----------------------------------------------------------------------
time_t lobo::SetDate(const std::string &s)
{
  // Default values
  int day = 9,
      month = 2,
      year = 68;

  size_t p2 = s.find('/');

  if (p2 != std::string::npos)
    day = std::stoi( s.substr(0, p2) );

  size_t p1 = p2  + 1;
  p2 = s.find("/", p1);

  if (p2 != std::string::npos)
    month = std::stoi( s.substr(p1, p2 - p1) );

  year = std::stoi( s.substr(p2 + 1) );

  if (year < 100)
      year += (year < 50 ? 2000 : 1900);

  return SetDate(day,month,year);
}
//----------------------------------------------------------------------
// Today's time_t
//----------------------------------------------------------------------
time_t lobo::Today()
{
  time_t now;
  time(&now);
  return now;
}
//----------------------------------------------------------------------
// Extract date from time_t
//----------------------------------------------------------------------
void lobo::GetDate(time_t t, int &day, int &month, int &year)
{
  tm *timeinfo = localtime(&t);  // See tm contents in the top of this file

  day = timeinfo->tm_mday;
  month = timeinfo->tm_mon + 1;
  year = timeinfo->tm_year + 1900;
}
//----------------------------------------------------------------------
// Extract time from time_t
//----------------------------------------------------------------------
void lobo::GetTime(time_t t, int &hour, int &min, int &sec)
{
  tm *timeinfo = localtime(&t);  // See tm contents in the top of this file

  hour = timeinfo->tm_hour;
  min = timeinfo->tm_min;
  sec = timeinfo->tm_sec;
}
//----------------------------------------------------------------------
// Extract date and time from time_t
//----------------------------------------------------------------------
void lobo::GetDateTime(time_t t, int &day, int &month, int &year,
											 int &hour, int &min, int &sec)
{
  tm *timeinfo = localtime(&t);  // See tm contents in the top of this file

  day = timeinfo->tm_mday;
  month = timeinfo->tm_mon + 1;
  year = timeinfo->tm_year + 1900;
  hour = timeinfo->tm_hour;
  min = timeinfo->tm_min;
  sec = timeinfo->tm_sec;
}
//----------------------------------------------------------------------
// Return the week day (0 = Sunday)
//----------------------------------------------------------------------
int lobo::GetWeekDay(time_t t)
{
  tm *timeinfo = localtime(&t);  // See tm contents in the top of this file
  int  day = timeinfo->tm_wday;
  return day;
}
//----------------------------------------------------------------------
// Return the month (January is 1)
//----------------------------------------------------------------------
int lobo::GetMonth(time_t t)
{
  tm *timeinfo = localtime(&t);  // See tm contents in the top of this file
  int month = timeinfo->tm_mon + 1;
  return month;
}
//----------------------------------------------------------------------
// Return the year
//----------------------------------------------------------------------
int lobo::GetYear(time_t t)
{
  tm *timeinfo = localtime(&t);  // See tm contents in the top of this file
  int year = timeinfo->tm_year + 1900;
  return year;
}
//----------------------------------------------------------------------
// Previous Monday or day if it is a Monday
//----------------------------------------------------------------------
lobo::YMD lobo::WeekBegin(time_t t)
{
  tm *timeinfo = localtime(&t); // See tm contents in the top of this file

  int wday = timeinfo->tm_wday; // Recovers the week day and...
  if (wday == 0) wday = 7;      // ...sets Sunday as day 7, not 0.
  wday -= 1;                    // Number of days from previous Monday

  timeinfo->tm_mday -= wday;    // Subtracts the difference from current day
  mktime(timeinfo);             // Creates the new time

  int y = timeinfo->tm_year + 1900,
      m = timeinfo->tm_mon + 1,
      d = timeinfo->tm_mday;

  return ( lobo::YMD {y, m, d} );   // c++11 only
}
//----------------------------------------------------------------------
// Create a string in the format YYYYMMDD
//----------------------------------------------------------------------
std::string lobo::YYYYMMDD(time_t t)
{
  int d, m, y;
  GetDate(t, d, m, y);
  std::string st = std::to_string(y) +
                  (m < 10 ? "0" : "") + std::to_string(m) +
                  (d < 10 ? "0" : "") + std::to_string(d);
  return st;
}
//----------------------------------------------------------------------
// YYYYMMDD string
//----------------------------------------------------------------------
std::string lobo::YYYYMMDD(const YMD &ymd)
{
  int m = std::get<1>(ymd),
      d = std::get<2>(ymd);

  std::string st = std::to_string(std::get<0>(ymd)) +
                  (m < 10 ? "0" : "") + std::to_string(m) +
                  (d < 10 ? "0" : "") + std::to_string(d);
  return st;
}
//----------------------------------------------------------------------
// Create a string in the format DD/MM/YYYY
//----------------------------------------------------------------------
std::string lobo::DDMMYYYY(time_t t)
{
  int d, m, y;
  GetDate(t, d, m, y);
  std::string st = (d < 10 ? "0" : "") + std::to_string(d) +
                   (m < 10 ? "/0" : "/") + std::to_string(m) +
                   "/" + std::to_string(y);
  return st;
}
//----------------------------------------------------------------------
// YYYYMMDD string
//----------------------------------------------------------------------
std::string lobo::DDMMYYYY(const YMD &ymd)
{
  int m = std::get<1>(ymd),
      d = std::get<2>(ymd);

  std::string st = (d < 10 ? "0" : "") + std::to_string(d) +
                   (m < 10 ? "/0" : "/") + std::to_string(m) +
                   "/" + std::to_string(std::get<0>(ymd));
  return st;
}
//----------------------------------------------------------------------
// DD/MM string (drops the year)
//----------------------------------------------------------------------
std::string lobo::DDMM(const YMD &ymd)
{
  int m = std::get<1>(ymd),
      d = std::get<2>(ymd);

  std::string st = (d < 10 ? "0" : "") + std::to_string(d) +
                   (m < 10 ? "/0" : "/") + std::to_string(m);
  return st;
}
//----------------------------------------------------------------------
// Create a string in the format hh:mm:ss
//----------------------------------------------------------------------
std::string lobo::hhmmss(unsigned sec)
{
  unsigned s = sec;                   // Convert sec to unsigned
  unsigned h = unsigned( s / 3600 );  // Calculate the number of hours
  s %= 3600;                          // Keep the remaining seconds
  unsigned m = unsigned ( s / 60 );   // Calculate the number of minutes
  s %= 60;                            // Keep the remaining seconds

  std::string st = std::to_string(h) +
                   (m < 10 ? ":0" : ":") + std::to_string(m) +
                   (s < 10 ? ":0" : ":") + std::to_string(s);

  return st;
}
//----------------------------------------------------------------------
// Create a string in the format mmm:ss
//----------------------------------------------------------------------
std::string lobo::mmmss(unsigned sec)
{
  unsigned m = unsigned ( sec / 60 );   // Calculate the number of minutes
  unsigned s = sec % 60;                // Keep the remaining seconds

  std::string st = std::to_string(m) +
                   (s < 10 ? ":0" : ":") + std::to_string(s);

  return st;
}
//----------------------------------------------------------------------
// Create a string in the format mmm:ss.xxx
//----------------------------------------------------------------------
std::string lobo::mmmssxxx(double min)
{
  unsigned m(min);
  double s = 60. * (min - m);

  std::stringstream ss;
  ss << m << (s < 10 ? ":0" : ":")
     << std::setiosflags(std::ios::fixed) << std::setprecision(3) << s;

  return ss.str();
}
