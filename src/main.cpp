//----------------------------------------------------------------------
// co(uch)po(tato) -- Program to track statistics on running, cycling
// and swimming.
// (c) R.P.S.M. Lobo -- rpsml@yahoo.com
//----------------------------------------------------------------------
#include <iostream>
#include <string>
//----------------------------------------------------------------------
#include "cpfuncs.h"
//----------------------------------------------------------------------
using namespace std;
//----------------------------------------------------------------------
// Main function
//----------------------------------------------------------------------
int main(int argc, char *argv[])
{
  cout << "-------------------------" << endl
       << "Couch Potato" << endl
       << "Version: Edzell Blue" << endl
       << "SN: 6.0"               << endl
       << "-------------------------" << endl;

  //
  // Parses command line arguments
  //
  if (argc != 1)      // There is at least one argument passed in addition to the program name
  {
    string arg = argv[1];

    if (arg == "-c" || arg == "--command")  // User wants to parse a command file
    {
      if (argc == 3)          // Expects command file name
        return ParseCommandFile(argv[2]);
    }
    else if (arg == "-t" || arg == "--terminal")  // User wants to change terminal in script file
    {
      if (argc == 4)                        // No file extension
        return ChangeTerminal(argv[2], argv[3]);
      else if (argc == 5)                   // File extension was also passed
        return ChangeTerminal(argv[2], argv[3], argv[4]);
    }
  }

  //
  // Error parsing the command line
  //
  cout << endl;
  cout << "Usage: coupo [flag] [arguments]" << endl
       << endl
       << "  -c, --command  Takes one argument <file>, a file with commands " << endl
       << "                 defining which data files and gnuplot scripts to" << endl
       << "                 generate."                                        << endl
       << endl
       << "  -t, --terminal Takes up to 3 arguements <script> <term> [ext]."  << endl
       << "                 Changes the <term>inal type in <script> as far"   << endl
       << "                 as the script had a terminal section. The"        << endl
       << "                 optional [ext] parameter sets the extension of"   << endl
       << "                 the file to save the Figure. If absent no file"   << endl
       << "                 is saved. In this case, sets each terminal with"  << endl
       << "                 a different number."                              << endl
       << endl
       << "Only one flag can be passed at a time."                            << endl
       << endl;

  return 1;
}
