//----------------------------------------------------------------------
// Class definition for an ensemble of exercises
// (c) R.P.S.M. Lobo -- rpsml@yahoo.com
//----------------------------------------------------------------------
#ifndef FITNESS_H
#define FITNESS_H
//----------------------------------------------------------------------
#include <iostream>
#include <limits>
#include <map>
#include <vector>
//----------------------------------------------------------------------
#include "exercise.h"
//----------------------------------------------------------------------
class fitness
{
  public:
    // Export options
    enum
    {
      xpDate        = 0x00000001,   // Date (default DD/MM/YYYY)
      xpUnixDate    = 0x00000002,   // Modifies date to Unix date
      xpDuration    = 0x00000004,   // Duration (default hh:mm:ss)
      xpDuration_M  = 0x00000008,   // Modifies duration to mm:ss
      xpAct         = 0x00000010,   // Action (default to text)
      xpActCode     = 0x00000020,   // Modifies action to code
      xpLocation    = 0x00000040,   // Location of exercise
      xpNote        = 0x00000080,   // Note about exercise
      xpDist        = 0x00000100,   // Distance (default km)
      xpDist_M      = 0x00000200,   // Modifies distance to m
      xpCompet      = 0x00000400,   // Competition (0 or 1)
      xpHRMAX       = 0x00000800,   // Maximum heart rate
      xpHRAVE       = 0x00001000,   // Average heart rate
      xpWeight      = 0x00002000,   // Weight (default kg)
      xpWeight_G    = 0x00004000,   // Modifies weight to g
      xpPace        = 0x00008000,   // Pace in mm:ss / km
      xpSpeed       = 0x00010000,   // Speed in km/h
      xpHRKm        = 0x00020000,   // Beats / km
      xpEquipment   = 0x00040000,   // Equipement used in exercise
      xpAllDefaults = 0x0007BDD5,   // All quantities in default format
      xpAll         = 0x0007FFFF    // All quantities in modified format
    };

    fitness()  {}    // Default constructor
    ~fitness() {}    // Destructor

    // Add a new item to the activities vector
    void Add(time_t TimeStamp,
             unsigned Act,
             const std::string &Equip,
             const std::string &Location,
             const std::string &Note,
             bool Competition,
             double Dist, unsigned Duration,
             unsigned HRMAX, unsigned HRAVE,
             double Weight);
    void Add(const exercise &activity);

    // Remove 'count' items starting at position index. If any element
    // is out of bounds returns false without removing anything.
    bool Remove(size_t index, size_t count = 1);

    // Top and bottom 'n' exercises by speed and distance
    void Sort();
    const std::string Last(unsigned exercise = al::run, size_t n = 5) const;
    const std::string Fastest(unsigned exercise = al::run, size_t n = 5) const;
    const std::string Slowest(unsigned exercise = al::run, size_t n = 5) const;
    const std::string Longest(unsigned exercise = al::run, size_t n = 5) const;
    const std::string Shortest(unsigned exercise = al::run, size_t n = 5) const;

    // Export
    void CustomHeader(std::ostream &os, unsigned Options);
    void CustomStream(std::ostream &os,
                      unsigned Options, const exercise &e);
    bool Export(const std::string &FileName, unsigned Options,
                size_t index = 0, size_t count = MAX_SIZE_T);

    time_t FindFirstEquip(const std::string &eq) const;   // First day an equipment was used
    time_t FindLastEquip(const std::string &eq) const;    // Last day an equipment was used

    // Read and write files
    bool ReadFile(const std::string &FileName);
    bool WriteFile(const std::string &FileName);

    const std::vector<exercise> &Exercises() const { return acts; }

    // [] operator and const version
    const exercise &operator[](size_t i) const { return acts[i]; }
    exercise &operator[](size_t i) { return acts[i]; }

    // Stream operators
    friend std::ostream &operator<<(std::ostream &os, const fitness &f);

    fitness(const fitness &)           = delete;  // Disable copy constructor
    fitness &operator=(const fitness&) = delete;  // Disable assignement

  private:
    static const size_t MAX_SIZE_T = std::numeric_limits<size_t>::max();
    std::vector<exercise> acts;

    // Sorting indexes by activity and speed/distance
    std::vector<size_t> RunSpeed,
                        RunDistance,
                        SwimSpeed,
                        SwimDistance,
                        CycleSpeed,
                        CycleDistance;
};
#endif  // FITNESS_H
