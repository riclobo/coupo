//----------------------------------------------------------------------
// Date and time conversion functions
// (c) R.P.S.M. Lobo -- rpsml@yahoo.com
//----------------------------------------------------------------------
#ifndef DATE_TIME_H
#define DATE_TIME_H
//----------------------------------------------------------------------
#include <ctime>
#include <string>
#include <tuple>
//----------------------------------------------------------------------
namespace lobo
{
  typedef std::tuple<int,int,int> YMD;

	// Create a time_t structure from date and time
	time_t SetDate(int day, int month, int year,
                 int hour = 12, int min = 0, int sec = 0);
  time_t SetDate(const std::string &s);

  // Today's time_t
  time_t Today();

	// Extract date and time from time_t
	void GetDate(time_t t, int &day, int &month, int &year);
	void GetTime(time_t t, int &hour, int &min, int &sec);
	void GetDateTime(time_t t,
									 int &day, int &month, int &year,
									 int &hour, int &min, int &sec);
  int GetWeekDay(time_t t);
  int GetMonth(time_t t);
  int GetYear(time_t t);

  YMD WeekBegin(time_t t);          // Previous Monday or day if it is a Monday

	std::string YYYYMMDD(time_t t);		      // YYYYMMDD string
  std::string YYYYMMDD(const YMD &ymd);   // YYYYMMDD string
	std::string DDMMYYYY(time_t t);		      // DD/MM/YYYY string
  std::string DDMMYYYY(const YMD &ymd);   // DD/MM/YYYY string
  std::string DDMM(const YMD &ymd);       // DD/MM string (drops the year)

  std::string hhmmss(unsigned sec);   // hh:mm:ss
  std::string mmmss(unsigned sec);    //   mmm:ss
  std::string mmmssxxx(double min);   // mmm:ss.xxx

}
#endif // DATE_TIME_H
