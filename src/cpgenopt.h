//----------------------------------------------------------------------
// Couch potato general options struct
//----------------------------------------------------------------------
#ifndef CPGENOPT_H
#define CPGENOPT_H
//----------------------------------------------------------------------
#include <queue>
#include <string>
#include <unordered_set>
//----------------------------------------------------------------------
#include "aliases.h"
#include "clparser.h"
//----------------------------------------------------------------------
struct OneLiners
{
  bool Show;

  unsigned Activity;

  size_t Last_n,
         Fastest_n,
         Longest_n;
};

struct OneLinerSummary
{
  OneLiners Run   {false, al::run,   10, 10, 10},   // One liners for running...
            Swim  {false, al::swim,  10, 10, 10},   // ...swimming...
            Cycle {false, al::cycle, 10, 10, 10};   // ...and cycling.
};

struct cpGenOpt
{
  // Constructors:
  // The empty constructor just creates a basic placeholder
  // The second fills in the struct members based on a series of strings
  // placed in a FIFO queue (command...value...command...value...)
  cpGenOpt() { }
  cpGenOpt(clparser::strqueue &q);
  bool HasOneLiner() { return Summary.Run.Show || Summary.Swim.Show || Summary.Cycle.Show; }

  bool        DontCallgp{false},          // Calls gnu plot when plots are done
              Report{false},              // Don't generate a report
              InvIg{false};               // Inverts behavior of the no parsing set

  std::string FileIn{"default.dat"},      // File containing data
              FilePrefix{""},             // Prefix added to all files
              FigFileExt{""},             // File extension to generate a figure
              gpTerm{"wxt"},              // gnuplot terminal
              gpTermOpt{""};              // gnuplot terminal options

  int         PlotLongSize{750};          // Longest size of the page
                                          // Landscape or portrait as
                                          // well as aspect ratio are
                                          // decided by the program.

  unsigned    Version{4};                 // Assumes gnuplot version 4


  std::unordered_set<int> NoParsing;      // Plots that should not be parsed

  OneLinerSummary Summary;                // Summary with one liners for each activity
};
//----------------------------------------------------------------------
#endif // CPGENOPT_H
