//----------------------------------------------------------------------
// Class to generate a gnuplot graphic
//----------------------------------------------------------------------
#include <fstream>
#include <sstream>
//----------------------------------------------------------------------
#include "gnuplotter.h"
//----------------------------------------------------------------------
using namespace std;
//----------------------------------------------------------------------
// Locally defined function -- Create axis format string
//----------------------------------------------------------------------
string FormatAxis(const gnuplotter::axis &a, unsigned gpVersion, unsigned xticsint = 0)
{
  stringstream ss;
  string       dq = "\"";

  // Normal or date/time data ?
  if (gpVersion < 5)                  // Up to version 4
    ss << "set " << a.ax << "data" << (a.ReadFmt.empty() ? "" : " time") << endl;
  else if (!a.ReadFmt.empty())    // Version 5 and on
  {
//    ss << "set " << a.ax << "data" << endl;
    ss << "set " << a.ax << "tics time" << endl;
    ss << "d" << a.ax << "(n)=timecolumn(n," << dq << a.ReadFmt << dq << ")" << endl;
  }

  // If date/time data, tells gnuplot how to read it
  if (!a.ReadFmt.empty() && (gpVersion < 5))
    ss << "set timefmt " << a.ax << " " << dq << a.ReadFmt << dq << endl;

  // Format for labels
  ss << "set format " << a.ax << " " << dq << a.WriteFmt << dq << endl;

  // Axis title (poorly named label in gnuplot)
  ss << "set " << a.ax << "label " << dq << a.Title << dq << endl;

  // The way to hide tics without hiding labels is to set their lenght to 0
  ss << "set " << a.ax << "tics scale " << ( a.ShowTics ? 1 : 0 ) << endl;

  // Rotate labels 90 degrees
  ss << "set " << a.ax << "tics " << ( a.Rotate ? "" : "no" ) << "rotate" << endl;

  // x axis specifics commands
  if (a.ax == 'x')
  {
    ss << "set xtics ";
    if (xticsint == 0)
      ss << "autofreq" << endl;
    else
      ss << xticsint << endl;
  }

  // y axis specifics commands
  if (a.ax == 'y')
  {
    ss << "set mytics 5" << endl;
  }

  return ss.str();
}
//----------------------------------------------------------------------
// Local function to return either column numbers or solum numbers with
// timecolum function.
//----------------------------------------------------------------------
string Column(int col, unsigned version, const gnuplotter::axis &a)
{
  if (version < 5)
    return to_string(col);

  if (a.ReadFmt.empty())
    return to_string(col);

  stringstream ss;
  ss << "(d" << a.ax << "(" << col << "))";
  return ss.str();
}
//----------------------------------------------------------------------
// Call this as many times as there are curves to add
//----------------------------------------------------------------------
void gnuplotter::AddCurve(const string &fn, const string &color,
                          const string &label, int colx, int coly,
                          bool lines, bool symbols, bool thick, bool smooth)
{
  stringstream st, pldef, pl;
  string dq  {"\""},
         sq  {"\'"};

  bool IsFunc = (fn[0] == '/');   // fn is a function not a file name

  // Defines linewidth and point size
  double lw = thick ? 4 : 1;
  double ps = thick ? 1 : 0.5;

  if (smooth) // Modifies lw and ps in the case of smopoth line superposition
  {
    lw = 2;
    ps = 0.08;
  }

  // Line style
  st << "set style line " << ( 1000 + CurrentPlot )
     << " lc rgb" << dq << color << dq
     << " lt 1"
     << " lw " << lw
     << " pt 7"
     << " ps " << ps;

  Styles.push_back(st.str());

  // Plot definitions
  pldef << PlotID << "dat" << CurrentPlot << "="
                  << dq;
  if (!IsFunc)
  {
    pldef         << sq << fn << sq
                  << " using " << Column(colx,gpVersion,xa) << ":" << Column(coly,gpVersion,ya) ;
  }
  else
  {
    pldef         << fn.substr(1);
  }
  pldef           << dq << endl
        << PlotID << "typ" << CurrentPlot << "="
                  << dq
                  << "with "
                  << (lines ? "lines" : "")
                  << (symbols ? "points" : "")
                  << dq << endl
        << PlotID << "sty" << CurrentPlot << "="
                  << dq
                  << "ls " << (1000 + CurrentPlot)
                  << (!label.empty() ? " title " : " notitle");

  if (!label.empty())
    pldef         << "\\" << dq << label << "\\" << dq;

  if (smooth)   // Adds smooth line on style section
    pldef         << " , \'\' using " << Column(colx,gpVersion,xa) << ":" << Column(coly,gpVersion,ya)
                  << " smooth bezier ls " << (1000 + CurrentPlot) << " notitle";

   pldef          << dq << endl;

  PlotDefs.push_back(pldef.str());

  // Plot command
  pl << "@" << PlotID << "dat" << CurrentPlot << " "
     << "@" << PlotID << "typ" << CurrentPlot << " "
     << "@" << PlotID << "sty" << CurrentPlot;

  Plots.push_back(pl.str());

  CurrentPlot++;//  for (unsigned i = 0; i <= PlotNo ; i++)

}
//----------------------------------------------------------------------
// Call this as many times as there are functions to add
//----------------------------------------------------------------------
void gnuplotter::AddFunc(const string &fn, const string &color,
                         const string &label,
                         bool lines, bool symbols, bool thick)
{
  AddCurve("/" + fn, color, label, 1, 1, lines, symbols, thick);
}
//----------------------------------------------------------------------
// Create the gnuplot commands
//----------------------------------------------------------------------
const string &gnuplotter::CreatePlot(const std::string &title)
{
  stringstream plot;

  string tab {"\\t"},
         dq  {"\""},
         sq  {"\'"},
         sep{datasep};

  // Comments at the top of the file
  plot << "#" << endl
       << "# File generated by gnuplotter class" << endl
       << "#" << endl
       << endl;

  // Preamble
  plot << "#" << endl
       << "# Preamble" << endl
       << "#" << endl
       << "set datafile separator " << dq <<  (datasep == '\t' ? tab : sep) << dq << endl
       << endl;

  if (!title.empty())
    plot << "set title " << dq << title << dq << endl;

  // Axes formatting
  plot << "#" << endl
       << "# Axes formatting" << endl
       << "#" << endl
       << FormatAxis(xa, gpVersion, xticsint) << endl
       << FormatAxis(ya, gpVersion) << endl;

  // Style
  plot << "#" << endl
       << "# Style" << endl
       << "#" << endl;

  for (auto &s : Styles) plot << s << endl;
  plot << endl;

  // Range, legend grid
  plot << "#" << endl
       << "# Range, legend, grid and what not" << endl
       << "#" << endl;

  plot << "set key maxrows 6" << endl;

  for (auto &s : Frame) plot << s << endl;
  plot << endl;

  // Plot definitions
  plot << "#" << endl
       << "# Plot definitions" << endl
       << "#" << endl;

  for (auto &s : PlotDefs) plot << s << endl;
  plot << endl;

  // Plot command
  plot << "#" << endl
       << "# Plot" << endl
       << "#" << endl
       << "plot ";

  for (auto i = Plots.begin(); i != Plots.end();)
  {
    plot << '\t' << (*i);       // Puts current item

    if ( ++i != Plots.end() )   // Increment iterator and check that it is not the end
      plot << ", \\";           // Puts continuation commads

    plot << endl;
  }

  if (!title.empty())
    plot << "unset title" << endl;

  gpcmd = plot.str();
  return gpcmd;
}
//----------------------------------------------------------------------
// Set the only histogram a plot can have. The last one set is kept.
//----------------------------------------------------------------------
void gnuplotter::SetHistogram(const std::string &fn,
                              const std::string &color,
                              int scale, bool NumericX)
{
  stringstream st, pldef, pl;
  string dq  {"\""},
         sq  {"\'"};

  // Style for hystogram
  st << "set boxwidth 0.75*" << scale << endl
     << "set style fill solid 0.5";

  Styles.push_back(st.str());

  // Plot definitions for histogram
  pldef << PlotID << "dat" << CurrentPlot << "="
                  << dq
                  << sq << fn << sq
                  << " using " << (NumericX ? "1:2" : "2:xtic(1)" )
                  << dq << endl
        << PlotID << "typ" << CurrentPlot << "="
                  << dq << "with boxes" << dq << endl
        << PlotID << "sty" << CurrentPlot << "="
                  << dq
                  << "lc rgb"
                  << "\\" << dq << color << "\\" << dq
                  << " notitle"
                  << dq << endl;

  PlotDefs.push_back(pldef.str());

  // Plot command
  pl << "@" << PlotID << "dat" << CurrentPlot << " "
     << "@" << PlotID << "typ" << CurrentPlot << " "
     << "@" << PlotID << "sty" << CurrentPlot;

  Plots.push_back(pl.str());

  CurrentPlot++;
}
//----------------------------------------------------------------------
// Set the only histogram a plot can have. The last one set is kept.
// This version is for a cluter histogram.
//----------------------------------------------------------------------
void gnuplotter::SetClusterHgram(const std::string &fn,                           // Data file name
                  const std::vector< std::pair<std::string,std::string> > &data) // {Caption,Color} for each plot
{
  stringstream st, pldef, pl;
  string dq  {"\""},
         sq  {"\'"};

  // Style for hystogram
  st << "n=" << data.size()                << "\t# Number of items" << endl
     << "bw=0.9"                           << "\t# Relative box width" << endl
     << "gap=0.15"                         << "\t# Gap between bars in a cluster" << endl
     << "dx=bw/(2*n)"                      << "\t# Half quantum of x shift for bars in a cluster" << endl
     << "set boxwidth (bw-gap)/n relative" << "\t# Width of individual bars" << endl
     << "set style fill solid 0.75";

  Styles.push_back(st.str());

  // Plot definitions for histogram
  int Counter = 0;
  int i0 = -( data.size() - 1 );    // Bar shifts go in steps of 2dx starting at -(n-1)
  for (auto &d : data)
  {
    pldef << PlotID << "dat" << CurrentPlot << Counter << "="
                    << dq
                    << sq << fn << sq
                    << " using ($1" << showpos << (i0 + 2 * Counter) << noshowpos << "*dx):" << Counter + 2
                    << dq << endl
          << PlotID << "typ" << CurrentPlot << Counter << "="
                    << dq << "with boxes" << dq << endl
          << PlotID << "sty" << CurrentPlot << Counter << "="
                    << dq
                    << "lc rgb"
                    << "\\" << dq << d.second << "\\" << dq
                    << " title \\\"" << d.first << "\\\""
                    << dq << endl;

    Counter++;
  }
  PlotDefs.push_back(pldef.str());

  // Plot command
  for (auto i = 0; i < Counter; i++)
  {
    pl.str(string());
    pl.clear();
    pl << '\t'
       << "@" << PlotID << "dat" << CurrentPlot << i << " "
       << "@" << PlotID << "typ" << CurrentPlot << i << " "
       << "@" << PlotID << "sty" << CurrentPlot << i;

    Plots.push_back(pl.str());
  }

  CurrentPlot++;
}
//----------------------------------------------------------------------
// Places the legend accordind to or'ed values of 0x01 = top; 0x02 = right; 0x04 = bottom; 0x08 = left
//----------------------------------------------------------------------
void gnuplotter::SetLegendPosition(unsigned pos)
{
  stringstream lp;

  string p1{"top"},
         p2{"right"};

  if (pos & al::top) p1 = "top";
  else if (pos & al::bottom) p1 = "bottom";

  if (pos & al::right) p2 = "right";
  else if (pos & al::left) p2 = "left";

  lp << "set key " << p1 << " " << p2;

  Frame.push_back(lp.str());
}
//----------------------------------------------------------------------
// Set numerical values for x range
//----------------------------------------------------------------------
void gnuplotter::SetRange(const gnuplotter::axis &a, int code, double low, double high)
{
  stringstream range;

  if (code == al::autosc)
  {
    range << "set autoscale " << a.ax;
  }
  else
  {
    if (gpVersion >= 5)
    {
      if (!a.ReadFmt.empty())
        range << "set timefmt " << "\"" << a.ReadFmt << "\"" << endl
              << "set " << a.ax << "data time" << endl;
      else
        range << "set " << a.ax << "data" << endl;
    }
    range << "set " << a.ax << "range[";
    if (code & al::low) range << low;
    range << ":";
    if (code & al::high) range << high;
    range << "]";

    if (!(code & al::low))
      range << endl << "set autoscale " << a.ax << "min";

    if (!(code & al::high))
      range << endl << "set autoscale " << a.ax << "max";
  }

  Frame.push_back(range.str());
}
//----------------------------------------------------------------------
// Set date/time values for x range
//----------------------------------------------------------------------
void gnuplotter::SetRange(const gnuplotter::axis &a, int code,
                          const string &date1, const string &date2)
{
  stringstream range;

  if (code == al::autosc)
  {
    range << "set autoscale " << a.ax;
  }
  else
  {
    if (gpVersion >= 5)
    {
      if (!a.ReadFmt.empty())
        range << "set timefmt " << "\"" << a.ReadFmt << "\"" << endl
              << "set " << a.ax << "data time" << endl;
      else
        range << "set " << a.ax << "data" << endl;
    }
    range << "set " << a.ax << "range[";
    if (code & al::low) range << "\"" << date1 << "\"";
    range << ":";
    if (code & al::high) range << "\"" << date2 << "\"";
    range << "]";

    if (!(code & al::low))
      range << endl << "set autoscale " << a.ax << "min";

    if (!(code & al::high))
      range << endl << "set autoscale " << a.ax << "max";
  }

  Frame.push_back(range.str());
}
//----------------------------------------------------------------------
// Shows a grid in the plot
// Major x: 0x01 / Minor x: 0x02 / Major y: 0X04 / Minor y: 0X08
//----------------------------------------------------------------------
void gnuplotter::ShowGrid(int Code,                   // Or'ed values for major and minor tics
                          const std::string &Mcolor,  // Major grid color
                          const std::string &mcolor)  // Minor grid color
{
  stringstream grid, st;
  string dq  {"\""};

  // Majors grid are to be shown
  if (Code & al::MX || Code & al::MY)   // Major x or y
  {
    // Sets major grid line style
    st << "set style line 2001 lt 0"
       << " lc rgb" << dq << Mcolor << dq
       << " lw 2" << endl;

    // Minor grid are to be shown
    if (Code & al::mx || Code & al::my) // Minor x or y
    {
      // Sets minor grid line style
      st << "set style line 2002 lt 0"
         << " lc rgb" << dq << mcolor << dq
         << endl;
    }
  }
  Styles.push_back(st.str());

  // Checks which x grid lines to show
  if (Code & al::MX)      // Major x
  {
    grid << "set grid xtics " << (Code & 0x02 ? "mxtics " : "")
         << "ls " << 2001
         << (Code & al::mx ? ", ls 2002" : "") <<  endl;
  }

  // Checks which y grid lines to show
  if (Code & al::MY)      // Major y
  {
    grid << "set grid ytics " << (Code & 0x08 ? "mytics ": "")
         << "ls " << 2001
         << (Code & al::my ? ", ls 2002" : "") <<  endl;
  }
  Frame.push_back(grid.str());
}
//----------------------------------------------------------------------
// Save gnuplot instructions to file
//----------------------------------------------------------------------
bool gnuplotter::Save(const string &FileName)
{
  ofstream FileOut(FileName);
  if (FileOut.fail()) return false;

  FileOut << *this;   // Writes gnuplot commands

  return true;
}
//----------------------------------------------------------------------
// Output stream
//----------------------------------------------------------------------
ostream &operator<<(ostream &os, const gnuplotter& gp)
{
  os << gp.gpcmd << endl;
  return os;
}
