//----------------------------------------------------------------------
// struct to parse a coupo layout queue
//----------------------------------------------------------------------
#ifndef CPLAYOUT_H
#define CPLAYOUT_H
//----------------------------------------------------------------------
#include <vector>
//----------------------------------------------------------------------
#include "clparser.h"
//----------------------------------------------------------------------
struct cpLayout
{
  struct OnePlot
  {
    bool Landscape,   // true is landscape mode, false is portrait
         ShowDate;    // Shows the data the plot was produced

    int Plot;         // Plot id
  };

  struct PlotList
  {
    bool Landscape,           // true is landscape mode, false is portrait
         ShowDate;

    int nr, nc;               // Number of rows and columns

    std::vector<int> Plots;   // List of plots to be shown
  };

  cpLayout() { };                       // Empty constructor - struct becomes just a place holder
  cpLayout( clparser::strqueue &q );    // Constructor with a layout queue

  std::vector<OnePlot>  Single;    // List of plots to be shown alone
  std::vector<PlotList> Grid;      // List of list of plots to be shown in a grid

  private:
    // Add a 'r'ow, 'c'olumn or 'g'rid layout
    void AddGridPlot(char c, const std::string &pars, bool Landscape, bool ShowDate);
};
//----------------------------------------------------------------------
#endif // CPLAYOUT_H
