//----------------------------------------------------------------------
// Cou(ch) Po(tato) functions
//----------------------------------------------------------------------
#ifndef CPFUNCS_H_INCLUDED
#define CPFUNCS_H_INCLUDED
//----------------------------------------------------------------------
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
//----------------------------------------------------------------------
#include "cpgenopt.h"
#include "cplayout.h"
#include "cpplot.h"
#include "fitness.h"
#include "gnuplotter.h"
#include "stats.h"
//----------------------------------------------------------------------
// Color definitions. The prefixes are (b)right, (d)ark, (l)ight
// Comments show suggestions on where to use them
//----------------------------------------------------------------------
namespace clr
{
  static std::string b_blue      {"#1E90FF"},  // Main plot & thick
                     b_orange    {"#FF4500"},  // Main plot & thick
                     b_yellow    {"#FFD700"},  // Thick
                     b_fuschia   {"#FF1493"},  // Histogram
                     b_green     {"#32CD32"},   // Thick
                     d_berillyum {"#66CDAA"},  // Thin
                     d_beige     {"#F4A460"},  // Thin
                     d_blue      {"#9FAFDF"},  // Thin
                     d_green     {"#006400"},  // Thin
                     d_plum      {"#DDA0DD"},  // Thin
                     d_violet    {"#483D8B"},  // Major grid
                     l_blue      {"#00BFFF"},  // Minor grid
                     coral       {"#FF7F50"},  // Thin
                     black       {"#000000"},  // Thin
                     red         {"#FF0000"},
                     blue        {"#0000FF"};

}
//----------------------------------------------------------------------
           int ChangeTerminal(const std::string &fn,
                              const std::string &term,
                              const std::string &ext = "");

          void CreateAxes(const std::string &xtype,
                          const std::string &ytype,
                          gnuplotter::axis &ax,
                          gnuplotter::axis &ay);

          void CreateLayouts(const std::string &fn,
                             const cpLayout &Layout,
                             const std::map<int,std::string> &lfns,
                             const cpGenOpt &g);

   std::string CreatePlot(int id,
                          const cpPlot &p,
                          const std::string &FPrfx,
                          const stats &st,
                          const fitness &fit);

          void gpScriptComp(const std::string & fn,
                            const cpPlot &p,
                            const gnuplotter::axis &ax,
                            const gnuplotter::axis &ay,
                            const stats &st,
                            gnuplotter &gp);

          void gpScriptDaily(const std::string &fn,
                             const cpPlot &p,
                             const gnuplotter::axis &ax,
                             const gnuplotter::axis &ay,
                             const fitness &fit,
                             gnuplotter &gp);

          void gpScriptEquip(const std::string & fn,
                             const cpPlot &p,
                             const gnuplotter::axis &ax,
                             const gnuplotter::axis &ay,
                             const stats &st,
                             gnuplotter &gp);

          void gpScriptTD(const std::string &fn,
                          const cpPlot &p,
                          const gnuplotter::axis &ax,
                          const gnuplotter::axis &ay,
                          const fitness &fit,
                          gnuplotter &gp);

          void gpScriptYear(const std::string & fn,
                            const cpPlot &p,
                            const gnuplotter::axis &ax,
                            const gnuplotter::axis &ay,
                            const stats &st,
                            gnuplotter &gp);

   std::string NewTerminal(unsigned id,
                           const cpGenOpt &g,
                           int w,
                           int h,
                           int font = 12);

           int ParseCommandFile(const std::string &fn);

          void Report(const std::string &rfn,
                      const stats &st,
                      const fitness &f,
                      const OneLinerSummary &ol);

          void WriteDailyData(const std::string &fn,
                              const cpPlot &p,
                              const fitness &fit);

          void WriteEquipmentData(const std::string &fn,
                                  const cpPlot &p,
                                  const stats &st);

          void WriteMonthData(const std::string &fn,
                              const cpPlot &p,
                              const stats &st,
                              double y1,
                              double y2);

std::ofstream & WriteNonZero(std::ofstream & os,
                             double d);

std::ofstream & WriteNonZero(std::ofstream & os,
                             unsigned d);

          void WriteTDScatter(const std::string &fn,
                              const cpPlot &p,
                              const fitness &fit,
                              double &ar,
                              double &as,
                              double &ac,
                              double &mr,
                              double &ms,
                              double &mc);

          void WriteWeekData(const std::string &fn,
                             const cpPlot &p,
                             const stats &st,
                             double y1,
                             double y2,
                             bool IncLineNo = false);

          void WriteYearlyData(const std::string &fn,
                               const cpPlot &p,
                               const stats &st);
//----------------------------------------------------------------------
#endif // CPFUNCS_H_INCLUDED
