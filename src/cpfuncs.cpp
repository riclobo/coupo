//----------------------------------------------------------------------
// Cou(ch) Po(tato) functions
//----------------------------------------------------------------------
#include "aliases.h"
#include "cpfuncs.h"
#include "datetime.h"
//----------------------------------------------------------------------
#include <algorithm>
#include <chrono>
#include <iomanip>
#include <iterator>
//----------------------------------------------------------------------
using namespace std;
//----------------------------------------------------------------------
// Change gnuplot terminal type in script file
//----------------------------------------------------------------------
int ChangeTerminal(const std::string &fn, const std::string &term,
                   const std::string &ext)
{
  string line;
  vector<string> file;

  ifstream filein(fn);      // Tries to open the script file

  if (!filein)              // Checks that the file was successfully open
  {
    cout << "Error: couldn't open " << fn << endl << endl;
    return 1;
  }

  // Reads the file in a vector of strings
  while (getline(filein, line))
    file.push_back(line);
  filein.close();

  // Searches for lines containing 'set output' or 'unset output
  for (auto &s : file)
  {
    size_t p = s.find("set output");   // Looks for 'set output ' (will also find 'unset output') string...

    if (p != string::npos)            // ...found it.
      s = "";                         // Clears the string
  }

  // Search for lines containing 'set term'
  int nn = 1;
  for (auto &s : file)
  {
    size_t p = s.find("set term ");   // Looks for 'set term ' string...

    if (p != string::npos)            // ...found it.
    {
      size_t q = s.find("size");
      string r = s.substr(q);

      // Creates new terminal command with output file
      stringstream ss;
      ss << "set term " << term << " ";

      if (ext.empty())
        ss << nn << " ";

      ss << " " << r << endl;

      if (!ext.empty())
        ss << "set output '" << "Fig" << nn << "." << ext << "'";

      s = ss.str();                   // Replaces line by new term
      nn++;
    }
  }

  file.push_back("unset output");   // Add a 'unset output' command at the end

  file.erase( unique(file.begin(), file.end()), file.end() );

  // Resaves (overwrites) the file with new terminals set in
  ofstream fileout(fn);
  copy(file.begin(), file.end(),  ostream_iterator<string>(fileout, "\n"));
  fileout.close();

  // Executes gnuplot
  string cmd = "gnuplot -persistent " + fn;
  if (system(cmd.data()) != 0)
  {
    cout << "Calling gnuplot failed!" << endl;
    return 1;
  }

  return 0;
}
//----------------------------------------------------------------------
// Create axes based on data types
//----------------------------------------------------------------------
void CreateAxes(const string &xtype, const string &ytype,
                gnuplotter::axis &ax, gnuplotter::axis &ay)
{
  if (xtype == "day")
    ax = gnuplotter::axis{'x', false, true, "Date", "%d/%m/%Y", "%d/%m/%y"};
  else if (xtype == "distance")
    ax = gnuplotter::axis{'x', false, true, "Distance (km)", "", "%g"};
  else if (xtype == "equipment")
    ax = gnuplotter::axis{'x', true, false, "Equipment", "", "%g"};
  else if (xtype == "week")
    ax = gnuplotter::axis{'x', false, true, "Date in Year", "%d/%m", "%b"};
  else if (xtype == "month")
    ax = gnuplotter::axis{'x', false, true, "Date in Year", "%m", "%b"};
  else if (xtype == "year")
    ax = gnuplotter::axis{'x', false, false, "Year", "", "%g"};

  if (ytype == "distance")
    ay = gnuplotter::axis{'y', false, true, "Distance (km)", "", "%g"};
  else if (ytype == "pace")
    ay = gnuplotter::axis{'y', false, true, "Pace (min / km)", "%M:%S", "%M:%S"};
  else if (ytype == "speed")
    ay = gnuplotter::axis{'y', false, true, "Speed (km/h)", "", "%g"};
  else if (ytype == "heart")
    ay = gnuplotter::axis{'y', false, true, "Average Heart Rate (beats / min)", "", "%g"};
  else if (ytype == "weight")
    ay = gnuplotter::axis{'y', false, true, "Weight (kg)", "", "%g"};
  else if (ytype == "time")
    ay = gnuplotter::axis{'y', false, true, "Time (hours)", "%H:%M:%S", "%H:%M"};
  else if (ytype == "events")
    ay = gnuplotter::axis{'y', false, true, "No. of Events", "", "%g"};
}
//----------------------------------------------------------------------
// Create Layouts -- Returns file name
//----------------------------------------------------------------------
void CreateLayouts(const string &fn, const cpLayout &Layout,
                   const map<int,string> &lfns, const cpGenOpt &g)
{
  ofstream  layf(fn);
  string today = lobo::DDMMYYYY(lobo::Today());

  unsigned  term{0};              // Terminal number

  int       w = g.PlotLongSize,   // Long page size
            h = 0.7 * w;          // Short page size

  // Top comment
  layf << "#" << endl
       << "# File created with Cou(ch) Po(tato)" << endl
       << "#" << endl << endl;

  // General settings
  layf << "#" << endl
       << "# General Settings" << endl
       << "#" << endl
       << "unset multiplot" << endl
       << "set macros" << endl << endl;

  // Single plots
  for (auto &op : Layout.Single)
  {
    bool b1 = ( g.NoParsing.find(op.Plot) != g.NoParsing.end() );     // Don't parse
    bool b2 = g.InvIg;                                                // Unless invert

    if ( b1 ^ b2 )   // Don't parse it
    {
      cout << "    Plot " << op.Plot << " ignored in General() section. Skipping layout creation." << endl;
    }
    else if (!lfns.at(op.Plot).empty())  // Runs through all, non empty, plot file names
    {
      // Comment indicating new page
      layf << "#" << endl
           << "# Plot terminal " << term << endl
           << "#" << endl;

      // Reset plot
      layf << "reset" << endl;

      // Create terminal and plot
      layf << NewTerminal(term, g, (op.Landscape ? w : h), (op.Landscape ? h : w)) << endl;

      // Save figure to file
      if (!g.FigFileExt.empty())
        layf << "set output \""
             << g.FilePrefix + "_Fig" + std::to_string(term) + g.FigFileExt
             << "\"" << endl;

      if (op.ShowDate)
        layf << "set label 1001 " << "\"" << today << "\" front at screen 0.01, screen 0.03 font \",6\"" << endl;

      layf << "load " << "\'" << lfns.at(op.Plot) << "\'" << endl;

      if (op.ShowDate)
        layf << "unset label 1001" << endl;

      if (!g.FigFileExt.empty())
        layf << "unset output" << endl;

      layf << endl;

      term++;   // Next terminal available
    }
    else      // File name was empty, the plot could not be created.
    {
      cout << "    Plot " << op.Plot << " could not be created. Skipping layout creation." << endl;
    }
  }

  // Row, Column and Grid plots
  for (auto &pl : Layout.Grid)
  {
    // Checks that plots were effectivelly created
    bool CanCreateLayout = true;
    string PlotsInLayout{"|"};

    for ( auto &i : pl.Plots )
    {
      bool b1 = ( g.NoParsing.find(i) != g.NoParsing.end() );     // Don't parse
      bool b2 = g.InvIg;                                          // Unless invert

      if ( b1 ^ b2 )   // Don't parse it
      {
        cout << "    Plot " << i << " ignored in General() section. Skipping layout creation." << endl;
        CanCreateLayout = false;
      }
      PlotsInLayout += to_string(i) + "|";
    }

    if (CanCreateLayout)
    {
      layf << "#" << endl
           << "# Multiplot terminal " << term << endl
           << "#" << endl;

      // Reset plot
      layf << "reset" << endl;

      // Create new terminal and open multiplot environement
      layf << NewTerminal(term, g, (pl.Landscape ? w : h), (pl.Landscape ? h : w), 10) << endl;

      // Save figure to file
      if (!g.FigFileExt.empty())
        layf << "set output \""
             << g.FilePrefix + "_Fig" + std::to_string(term) + g.FigFileExt
             << "\"" << endl;

      layf << "set multiplot layout " << pl.nr << "," << pl.nc << endl;
      layf << "#set lmargin XXX # Uncomment this line setting XXX to place all left margins at the same position" << endl
           << "#set bmargin YYY # Uncomment this line setting YYY to place all bottom margins at the same position" << endl;

      if (pl.ShowDate)
        layf << "set label 1001 " << "\"" << today << "\" front at screen 0.01, screen 0.03 font \",6\"" << endl;

      // Adds all plots that were effectively created
      for ( auto &i : pl.Plots )
        if (!lfns.at(i).empty())
          layf << "load " << "\'" << lfns.at(i) << "\'" << endl;

      if (pl.ShowDate)
        layf << "unset label 1001" << endl;

      layf << "unset multiplot" << endl;  // Closes the multiplot

      if (!g.FigFileExt.empty())
        layf << "unset output" << endl;

      layf << endl;

      term++;     // Next terminal available
    }
    else
    {
      cout << "Warning: Layout creation failed for Plots " << PlotsInLayout
           << " as at least one of these was not parsed." << endl;
    }
  }

  layf.close();     // Closes gnuplot main file
}
//----------------------------------------------------------------------
// Create a gnuplot command file -- Returns the generated file name
//----------------------------------------------------------------------
string CreatePlot(int id, const cpPlot &p,
                  const string &FPrfx,
                  const stats &st, const fitness &fit, unsigned Version)
{
  // axis ---> { axis, Rotate, ShowTics, Title, ReadFmt, WriteFmt }
  string basefn{FPrfx + "_Plot" + std::to_string(id)};

  // Defines and creates axis properties
  gnuplotter::axis ax, ay;
  CreateAxes(p.xDataType(), p.yDataType(), ax, ay);

  if (p.Normalize())
    ay.Title += " / Event";

  if (p.Accumulate())
      ay.Title = "Accumulated " +  ay.Title;

  // Creates the "gnu plotter" and shows the grid if requested
  gnuplotter gp(ax,ay,id,p.XTicInt(),Version);
  if (p.ShowGrid())
    gp.ShowGrid(al::fullgr, clr::d_violet, clr::l_blue);

  // Creates data files and formats gnuplotter
  if (p.xDataType() == "day")                                       // Daily data
    gpScriptDaily(basefn + ".dat", p, ax, ay, fit, gp);
  else if (p.xDataType() == "distance" && p.yDataType() == "time")  // Time - Distance plot
    gpScriptTD(basefn + ".dat", p, ax, ay, fit, gp);
  else if (p.xDataType() == "equipment")                            // Equipment data
    gpScriptEquip(basefn + ".dat", p, ax, ay, st, gp);
  else if (p.xDataType() == "week" || p.xDataType() == "month")     // Compare Week/Month plot
    gpScriptComp(basefn + ".dat", p, ax, ay, st, gp);
  else if (p.xDataType() == "year")                                 //Yearly plot
    gpScriptYear(basefn + ".dat", p, ax, ay, st, gp);

  // Creates the gnuplot script file
  ofstream gpf(basefn + ".gp");

  gpf << gp.CreatePlot(p.Title()) << endl;       // Writes all commands

  if (p.ShowGrid())                     // If the grid was set...
    gpf << "unset grid" << endl;        // ...unsets it.

  gpf.close();

  // Returns the name of the file just created
  return basefn + ".gp";
}
//----------------------------------------------------------------------
// Creates the gnuplot script for comparative graphics
//----------------------------------------------------------------------
void gpScriptComp(const std::string & fn, const cpPlot &p,
                  const gnuplotter::axis &ax, const gnuplotter::axis &ay,
                  const stats &st, gnuplotter &gp)
{
  // Checks for which years to add to vector
  string s = p.Range_i();
  int y1 = ( s.empty() ? st.Year().begin()->first : std::stoi(s) );

  s = p.Range_f();
  int y2 = ( s.empty() ? st.Year().rbegin()->first : std::stoi(s) );

  // Check binning size
  if ( p.xDataType() == "week" )
    WriteWeekData(fn, p, st, y1, y2, true); // Write weekly data with line number saving
  else if ( p.xDataType() == "month")
    WriteMonthData(fn, p, st, y1, y2);      // Write monthly data without line number saving

  vector<string> c {clr::b_blue,  clr::b_orange,  clr::b_yellow,
                    clr::b_green, clr::b_fuschia, clr::d_berillyum,
                    clr::d_beige, clr::d_blue,    clr::d_green,
                    clr::d_plum} ;

  // Adds curves
  for (int i = y1; i <= y2; i++)
  {
    int u1{0}, u2{0};       // Indexes for gnuplot 'using'

    // Check binning size
    if ( p.xDataType() == "week" )
    {
      u1 = 2 * (i - y1) + 1 + 1;        // In both lines, the second +1 accounts for the line...
      u2 = 2 * (i - y1) + 2 + 1;        // ...numbers in data file (avoids a problem with gnuplot)
    }
    else if ( p.xDataType() == "month")
    {
      u1 = 1;
      u2 = i - y1 + 2;
    }

    // Color index, the last curve has the first color and
    // it cycles over the 10 colors in decreasing order
    int ci = (y2 - i) % 10;

    gp.AddCurve(fn, c[ci], to_string(i), u1, u2,
                true, true,
                i > ( y2 - int(p.Highlight()) ) ? true : false);
  }

  // Check binning size
  if ( p.xDataType() == "week" )
    gp.SetRange(ax, al::low | al::high, "01/01", "31/12");  // Sets range as full year
  else if ( p.xDataType() == "month")
    gp.SetRange(ax, al::low | al::high, "1", "12");  // Sets range as full year

  gp.SetRange(ay, p.YAuto(), 0, 0);                // Set low y range
  gp.SetLegendPosition(p.LegendPos());    // Legent position
}
//----------------------------------------------------------------------
// Creates the gnuplot script for daily x axis
//----------------------------------------------------------------------
void gpScriptDaily(const string & fn, const cpPlot &p,
                   const gnuplotter::axis &ax, const gnuplotter::axis &ay,
                   const fitness &fit, gnuplotter &gp)
{
  WriteDailyData(fn, p, fit);

  if (p.yDataType() != "heart")
  {
    gp.AddCurve(fn, clr::b_blue, "", 1, 2, !p.Smooth(), p.Smooth() || p.ShowPoints(), false, p.Smooth());
  }
  else
  {
    if ( p.WhichHR() & al::ave )
    {
      gp.AddCurve(fn, clr::b_blue, "< HR >", 1, 2, !p.Smooth(), p.Smooth() || p.ShowPoints(), false, p.Smooth());
      if ( p.WhichHR() & al::max )
        gp.AddCurve(fn, clr::b_orange, "Max HR", 1, 3, !p.Smooth(), p.Smooth() || p.ShowPoints(), false, p.Smooth());
    }
    else if ( p.WhichHR() & al::max )
    {
      gp.AddCurve(fn, clr::b_orange, "Max HR", 1, 2, !p.Smooth(), p.Smooth() || p.ShowPoints(), false, p.Smooth());
    }
  }

  int code = (p.Range_i().empty() ? al::autosc : al::low ) |
             (p.Range_f().empty() ? al::autosc : al::high);

  gp.SetRange(ax, code, p.Range_i(), p.Range_f());
  gp.SetRange(ay, p.YAuto(), 0, 0);   // Set low y range
  gp.SetLegendPosition(p.LegendPos());    // Legent position
}
//----------------------------------------------------------------------
// Creates the gnuplot script for equipement x axis
//----------------------------------------------------------------------
void gpScriptEquip(const string & fn, const cpPlot &p,
                   const gnuplotter::axis &ax, const gnuplotter::axis &ay,
                   const stats &st, gnuplotter &gp)
{
  if (p.yDataType() == "distance")
  {
    WriteEquipmentData(fn, p, st);
    gp.SetHistogram(fn, clr::b_fuschia, 1, false);
    gp.SetRange(ax, al::autosc, 0, 0);
    gp.SetRange(ay, p.YAuto(), 0, 0);
    gp.SetLegendPosition(p.LegendPos());    // Legent position
  }
}
//----------------------------------------------------------------------
// Creates the time-distance gnuplot script
//----------------------------------------------------------------------
void gpScriptTD(const string & fn, const cpPlot &p,
                const gnuplotter::axis &ax, const gnuplotter::axis &ay,
                const fitness &fit, gnuplotter &gp)
{
  // Writes the data and calculates regression coefficients
  double ar, as, ac,  // Fitting parameters for running, swimming and cycling
         mr, ms, mc;  // Masimum distances ran, swimmed and cycled
  WriteTDScatter(fn, p, fit, ar, as, ac, mr, ms, mc);

  if (p.Exercise() & al::run)
  {
    string func = "( x < " + std::to_string(mr) + " ? "   // Defines a function
                           + std::to_string(ar) + " * x : 1/0 )";
    stringstream ss;
    ss << lobo::mmmss(ar) << " min/km ("
       << setiosflags(ios::fixed) << setprecision(2)
       << (3600./ar) << " km/h)";
    gp.AddCurve(fn, clr::b_green, "Run", 1, 2, false, true);
    gp.AddFunc(func, clr::d_green, ss.str());
  }

  if (p.Exercise() & al::swim)
  {
    unsigned sh = (p.Exercise() == al::all ? 2 : 0);      // Shift in data file column position
    string func = "( x < " + std::to_string(ms) + " ? "   // Defines a function
                           + std::to_string(as) + " * x : 1/0 )";
    stringstream ss;
    ss << lobo::mmmss(as/10.) << " min/100m ("
       << setiosflags(ios::fixed) << setprecision(2)
       << (3600./as) << " km/h)";
    gp.AddCurve(fn, clr::b_blue, "Swim", 1 + sh, 2 + sh, false, true);
    gp.AddFunc(func, clr::blue, ss.str());
  }

  if (p.Exercise() & al::cycle)
  {
    unsigned sh = (p.Exercise() == al::all ? 4 : 0);      // Shift in data file column position
    string func = "( x < " + std::to_string(mc) + " ? "   // Defines a function
                           + std::to_string(ac) + " * x : 1/0 )";
    stringstream ss;
    ss << lobo::mmmss(ac) << " min/km ("
       << setiosflags(ios::fixed) << setprecision(2)
       << (3600./ac) << " km/h)";
    gp.AddCurve(fn, clr::coral, "Cycle", 1 + sh, 2 + sh, false, true);
    gp.AddFunc(func, clr::b_orange, ss.str());
  }

  gp.SetRange(ax, al::low, 0, 0);
  gp.SetRange(ay, al::low, 0, 0);        // Set low y range
  gp.SetLegendPosition(p.LegendPos());    // Legent position

}
//----------------------------------------------------------------------
// Creates the yearly gnuplot script
//----------------------------------------------------------------------
void gpScriptYear(const std::string & fn, const cpPlot &p,
                  const gnuplotter::axis &ax, const gnuplotter::axis &ay,
                  const stats &st, gnuplotter &gp)
{
  if (p.yDataType() == "distance" || p.yDataType() == "events")
  {
    WriteYearlyData(fn, p, st);
    std::vector< std::pair<std::string,std::string> > data;

    // Captions and colors
    if (p.Multi() & al::all_inc)              // Include total activities in year
      data.push_back({"All",clr::red});

    if (p.Multi() & al::run)                  // Include running stats
      data.push_back({"Run",clr::coral});

    if (p.Multi() & al::cycle)                // Include cycling stats
      data.push_back({"Cycle",clr::b_orange});

    if (p.Multi() & al::swim)                 // Include swimming stats
      data.push_back({"Swim",clr::blue});

    gp.SetClusterHgram(fn,data);        // Sets the clustered histogram

    int code = (p.Range_i().empty() ? al::autosc : al::low ) |
               (p.Range_f().empty() ? al::autosc : al::high);

    gp.SetRange(ax, code, p.Range_i(), p.Range_f());
    gp.SetRange(ay, p.YAuto(), 0, 0);   // Set low y range
    gp.SetLegendPosition(p.LegendPos());    // Legent position
  }
}
//----------------------------------------------------------------------
// Generate a string for a new gnuplot terminal
//----------------------------------------------------------------------
string NewTerminal(unsigned id, const cpGenOpt &g, int w, int h, int font)
{
  stringstream nt;

  // Creates a new terminal with the proper options, orientation and sizes
  nt << "set term " << g.gpTerm << " " << id << " "
     << g.gpTermOpt
     << " size " << w << "," << h
     << " font \"," << font << "\"";

  return nt.str();
}
//----------------------------------------------------------------------
// Parses a command file into gnuplot scripts
//----------------------------------------------------------------------
int ParseCommandFile(const std::string &fn)
{
  // Start time counting
  chrono::steady_clock::time_point starttime = chrono::steady_clock::now();

  //
  // Parses the command file
  //
  clparser cp(std::cout, false);
  bool success = cp.Parse(fn);

  // Parsing messages
  cout << "** MESSAGES **" << endl;
  cp.ParsingMessages(std::cout);
  cout << endl;

  // If parsing did not succeed, shows errors
  if (!success)
  {
    cout << "** ERRORS **" << endl;
    cp.ParsingErrors(std::cout);
    return 1;
  }

  // Time milestone
  chrono::steady_clock::time_point parsetime = chrono::steady_clock::now();

  //
  // Parses the queues
  //
  cpGenOpt Gen( cp.Queue("general()") );    // Parses General options queue

  // Parses Plot queues
  cout << "Parsing plots..." << endl;

  map<int,cpPlot> Plots;

  for (auto &s : cp.Blocks())
  {
    size_t p = s.find("plot");    // Checks if Block is a plot...

    if (p != string::npos)        // ...it is!!
    {
      size_t i = s.find("(");                               // Looks for parenthesis
      std::string r = s.substr(i + 1, s.length() - i - 2);  // Looks for plot number
      i = r != "" ? std::stoi(r) : 0;                       // Finds plot number

      bool b1 = ( Gen.NoParsing.find(i) == Gen.NoParsing.end() ); // Parse it
      bool b2 = Gen.InvIg;                                        // Unless invert

      if ( b1 ^ b2 )        // Parse it
      {
        cpPlot p( cp.Queue(s) );    // Parses the plot (parsing checks for duplicates)

        if (!p.Errors().empty())    // Errors messages exist
        {
          cout << "Error parsing Plot(" << i << ")" << endl
               << p.Errors() << endl;
        }
        else
          Plots[i] = cpPlot( p );   // No errors, adds it to map
      }
      else
      {
        cout << "    Ignoring plot " << i << endl;
      }
    }
  }

  cout << "Parsing layouts..." << endl;

  cpLayout Layout( cp.Queue("layout()") );    // Parses Layout queue

  // Time milestone
  chrono::steady_clock::time_point queuetime = chrono::steady_clock::now();

  //
  // Statistics
  //
  cout << "Generating Statistics..." << endl;
  fitness fit;                // Creates fitness object
  fit.ReadFile(Gen.FileIn);   // Reads the data file
  fit.Sort();                 // Sorts the fitting object
  stats st(fit);              // Calculates statistics

  if (Gen.Report)
    Report(Gen.FilePrefix + "_Report.txt", st, fit, Gen.Summary);    // Generate an overall report
  else if (Gen.HasOneLiner())
      cout << endl << ">>> WARNING: OneLiner() called in \'" << fn << "\' without calling Report() <<<" << endl << endl;

  // Time milestone
  chrono::steady_clock::time_point stattime = chrono::steady_clock::now();

  // Generate gnuplot plots
  cout << "Creating plots..." << endl;
  map<int,string> PlotNames;


  for (auto &Plot : Plots)
    PlotNames[Plot.first] = CreatePlot(Plot.first, Plot.second, Gen.FilePrefix, st, fit, Gen.Version);

  // Create layouts
  cout << "Creating layouts..." << endl;
  string MainFile = Gen.FilePrefix + "_main.gp";

  if (!PlotNames.empty())         // Checks that there were sucessful plot parsings
    CreateLayouts(MainFile, Layout, PlotNames, Gen);

  // Time milestone
  chrono::steady_clock::time_point gnutime = chrono::steady_clock::now();

  // Show times
  cout << endl << setiosflags(ios::fixed) << setprecision(3)
       << "Total running time:       " << setw(12) << chrono::duration_cast<chrono::microseconds>( gnutime   - starttime ).count() / 1000. << " ms" << endl
       << "Configuration parsing:    " << setw(12) << chrono::duration_cast<chrono::microseconds>( parsetime - starttime ).count() / 1000. << " ms" << endl
       << "Queues parsing:           " << setw(12) << chrono::duration_cast<chrono::microseconds>( queuetime - parsetime ).count() / 1000. << " ms" << endl
       << "Statistical calculations: " << setw(12) << chrono::duration_cast<chrono::microseconds>( stattime  - queuetime ).count() / 1000. << " ms" << endl
       << "gnuplot scripts & data:   " << setw(12) << chrono::duration_cast<chrono::microseconds>( gnutime   - stattime  ).count() / 1000. << " ms" << endl;

  if (Gen.DontCallgp)   // Don't call gnuplot
  {
    cout << endl << ">>> WARNING: DontCallgp() flag is set in \'" << fn << "\' <<<" << endl << endl;
  }
  else
  {
    // Calls gnuplot
    string cmd = "gnuplot -persistent " + MainFile;
    if (system(cmd.data()) != 0)
    {
      cout << "Calling gnuplot failed!" << endl;
      return 1;
    }
  }

  return 0;
}
//----------------------------------------------------------------------
// Generate an overall report
//----------------------------------------------------------------------
void Report(const string &rfn, const stats &st, const fitness &f, const OneLinerSummary &ol)
{
  ofstream rf(rfn);

  rf << "*********************************" << endl;
  rf << "*****   GLOBAL STATISTICS   *****" << endl;
  rf << "*********************************" << endl;

  rf << st.Global() << endl;

  rf << "************************************" << endl;
  rf << "*****   FASTEST PERFORMANCES   *****" << endl;
  rf << "************************************" << endl;

  rf << st.Fastest() << endl;

  rf << "************************************" << endl;
  rf << "*****   SLOWEST PERFORMANCES   *****" << endl;
  rf << "************************************" << endl;

  rf << st.Slowest() << endl;


  if (ol.Run.Show)
  {
    rf << "************************************" << endl;
    rf << "*****   RUNNING PERFORMANCES   *****" << endl;
    rf << "************************************" << endl;

    if (ol.Run.Last_n > 0)
    {
      rf << "-----   Last " << ol.Run.Last_n << "  -----" << endl;
      rf << f.Last(al::run, ol.Run.Last_n) << endl;
    }

    if (ol.Run.Fastest_n > 0)
    {
      rf << "-----   Fastest " << ol.Run.Fastest_n << "  -----" << endl;
      rf << f.Fastest(al::run, ol.Run.Fastest_n) << endl;
    }

    if (ol.Run.Longest_n > 0)
    {
      rf << "-----   Longest " << ol.Run.Longest_n << "  -----" << endl;
      rf << f.Longest(al::run, ol.Run.Longest_n) << endl;
    }
  }

  if (ol.Cycle.Show)
  {
    rf << "************************************" << endl;
    rf << "*****   CYCLING PERFORMANCES   *****" << endl;
    rf << "************************************" << endl;

    if (ol.Cycle.Last_n > 0)
    {
      rf << "-----   Last " << ol.Cycle.Last_n << "  -----" << endl;
      rf << f.Last(al::cycle, ol.Cycle.Last_n) << endl;
    }

    if (ol.Cycle.Fastest_n > 0)
    {
      rf << "-----   Fastest " << ol.Cycle.Fastest_n << "  -----" << endl;
      rf << f.Fastest(al::cycle, ol.Cycle.Fastest_n) << endl;
    }

    if (ol.Cycle.Longest_n > 0)
    {
      rf << "-----   Longest " << ol.Cycle.Longest_n << "  -----" << endl;
      rf << f.Longest(al::cycle, ol.Cycle.Longest_n) << endl;
    }
  }

  if (ol.Swim.Show)
  {
    rf << "*************************************" << endl;
    rf << "*****   SWIMMING PERFORMANCES   *****" << endl;
    rf << "*************************************" << endl;

    if (ol.Swim.Last_n > 0)
    {
      rf << "-----   Last " << ol.Swim.Last_n << "  -----" << endl;
      rf << f.Last(al::swim, ol.Swim.Last_n) << endl;
    }

    if (ol.Swim.Fastest_n > 0)
    {
      rf << "-----   Fastest " << ol.Swim.Fastest_n << "  -----" << endl;
      rf << f.Fastest(al::swim, ol.Swim.Fastest_n) << endl;
    }

    if (ol.Swim.Longest_n > 0)
    {
      rf << "-----   Longest " << ol.Swim.Longest_n << "  -----" << endl;
      rf << f.Longest(al::swim, ol.Swim.Longest_n) << endl;
    }
  }

  rf << "*******************************" << endl;
  rf << "*****   EQUIPMENT USAGE   *****" << endl;
  rf << "*******************************" << endl;

  st.ReportEquip(rf,4);

  rf << "*********************************" << endl;
  rf << "*****   YEARLY STATISTICS   *****" << endl;
  rf << "*********************************" << endl;

  for (auto &ct : st.Year() )
  {
    rf << "=== " << stats::Year(ct) << " ===" << endl;
    rf << stats::YearCat(ct) << endl;
  }

  rf.close();
}
//----------------------------------------------------------------------
// Write file with daily data
//----------------------------------------------------------------------
void WriteDailyData(const string &fn, const cpPlot &p, const fitness &fit)
{
  double v{0};    // Counter for accumulation
  ofstream df(fn);

  for ( auto &e : fit.Exercises() )
  {

    // Checks filter status
    bool IncludeThis = true;

    if (!p.Filter().empty())
    {
      auto it = p.Filter().begin();             // First element (this is the behavior flag

      // Only continues if term was not yet found. Increments it before doing anything else.
      while ( IncludeThis && (++it != p.Filter().end()) )
        IncludeThis = ( e.GetEquipment().find(*it) == string::npos &&
                        e.GetLocation().find(*it)  == string::npos &&
                        e.GetNote().find(*it)      == string::npos );

      if (p.Filter()[0] == "+")       // Filter should include, instead of exclude search terms
        IncludeThis = !IncludeThis;   // Reverses filter result
    }


    if ( IncludeThis && (p.Exercise() == al::all || e.GetExercise() == p.Exercise()) )
    {
      // Writes the same x for all
      df << lobo::DDMMYYYY(e.GetTimeStamp());

      // Writes y on a case-by-case basis
      if (p.yDataType() == "distance")
      {
        if (p.Accumulate())
          v += e.GetDistance();
        else
          v = e.GetDistance();
        df << '\t' << v << endl;
      }
      else if (p.yDataType() == "speed")
        df << '\t' << e.Speed() << endl;
      else if (p.yDataType() == "weight")
        WriteNonZero(df, e.GetWeight()) << endl;
      if (p.yDataType() == "pace")
        df << '\t' << lobo::mmmssxxx(e.Pace()) << endl;
      else if (p.yDataType() == "heart")
      {
        if (p.WhichHR() & al::ave)
          WriteNonZero(df, e.GetAveHR());
        if (p.WhichHR() & al::max)
          WriteNonZero(df, e.GetMaxHR());
        df << endl;
      }
    }
  }
  df.close();
}
//----------------------------------------------------------------------
// Write file with Equipement data
//----------------------------------------------------------------------
void WriteEquipmentData(const string &fn, const cpPlot &p, const stats &st)
{
  ofstream df(fn);  // Opens data file

  // Runs through all equipments in the order they were read
  for ( auto &s : st.EqOrd() )
  {
    const stats::counters &eq = st.Equip().at(s);   // Reference to equipement

    // Checks filter status
    bool IncludeThis = true;
    if (!p.Filter().empty())
    {
      auto it = p.Filter().begin();             // First element (this is the behavior flag

      // Only continues if term was not yet found. Increments it before doing anything else.
      while ( IncludeThis && (++it != p.Filter().end()) )
        IncludeThis = ( s.find(*it) == string::npos );

      if (p.Filter()[0] == "+")       // Filter should include, instead of exclude search terms
        IncludeThis = !IncludeThis;   // Reverses filter result
    }

    if ( IncludeThis && (p.Exercise() == al::all || eq.Exercise == p.Exercise()) )
    {
      double d = eq.Distance;

      if (p.Normalize())                  // If requested...
        d /= eq.Events;     // Normalizes data by number of events.

      df << s << '\t' << d << endl;
    }
  }
  df.close();
}
//----------------------------------------------------------------------
// Write file with monthly comparative data
// [Distance, Pace, Speed] is accumulated in [Week, Month]
//----------------------------------------------------------------------
void WriteMonthData(const std::string &fn, const cpPlot &p,
                    const stats &st, double y1, double y2)
{
  vector<double> OneYear;             // Creates a vector to hold montly data
  OneYear.assign(12,-1);              // Initializes vector with 12 negative values

  map< int, vector<double> > Conc;    // Table to store concomitant {year,months} data

  for (auto &yr : st.Year())          // Runs through all years
  {
    if (stats::Year(yr) >= y1 && stats::Year(yr) <= y2)   // Year is in the sought range
      Conc[stats::Year(yr)] = OneYear;                    // Inserts one full set of months per year
  }

  // Runs through all months
  for ( auto &mo : st.Month() )
  {
    if (stats::MonthYr(mo) >= y1 && stats::MonthYr(mo) <= y2)   // Year is in the sought range
    {
      // Initializes a reference according to the chosen exercise
      const stats::counters &ct = (p.Exercise() == al::run ?
                                                    stats::MonthCat(mo).Run :
                                                    p.Exercise() == al::cycle ?
                                                                     stats::MonthCat(mo).Cycle :
                                                                     stats::MonthCat(mo).Swim);

      // Loads the data according to Year and Month in Concomitant vector
      //
      // Distance
      //
      if (p.yDataType() == "distance" && ct.Distance > 0.)
      {
        //
        // Accumulated distance
        //
        if (p.Accumulate())
        {
          if (stats::MonthMo(mo) > 1)     // This is not January
          {
            double prev = Conc[stats::MonthYr(mo)][stats::MonthMo(mo)-2];   // Previous month data

            if (prev < 0) // There was no data set in the previous month.
            {
              if (stats::MonthMo(mo) > 2) // This is at least March -- Sets previous month to two months behind
              {
                Conc[stats::MonthYr(mo)][stats::MonthMo(mo)-2] = Conc[stats::MonthYr(mo)][stats::MonthMo(mo)-3];
                prev = Conc[stats::MonthYr(mo)][stats::MonthMo(mo)-3];  // Sets the data from two months behind
              }
              else                        // February -- Sets previous month (January) to 0
              {
                Conc[stats::MonthYr(mo)][stats::MonthMo(mo)-2] = 0;
                prev = 0;
              }
            }

            Conc[stats::MonthYr(mo)][stats::MonthMo(mo)-1] = prev + ct.Distance;  // Adds monthly data to previous month
          }
          else                            // This is January
          {
            Conc[stats::MonthYr(mo)][stats::MonthMo(mo)-1] = ct.Distance;   // First month data
          }
        }
        //
        // Normalized distance
        //
        else if (p.Normalize())
        {
          int n = ct.Events;
          if (n == 0) n = 1 ;
          Conc[stats::MonthYr(mo)][stats::MonthMo(mo)-1] = ct.Distance / n;
        }
        //
        // Plain distance
        //
        else
        {
          Conc[stats::MonthYr(mo)][stats::MonthMo(mo)-1] = ct.Distance;
        }
      }
      //
      // Pace
      //
      else if (p.yDataType() == "pace" && ct.Pace > 0.)
        Conc[stats::MonthYr(mo)][stats::MonthMo(mo)-1] = ct.Pace;
      //
      // Speed
      //
      else if (p.yDataType() == "speed" && ct.Speed > 0.)
        Conc[stats::MonthYr(mo)][stats::MonthMo(mo)-1] = ct.Speed;
    }
  }

  ofstream df(fn);  // Opens data file

  // Saves data according to Month order
  for (size_t m = 0; m < 12; m++)
  {
    df << m + 1;                  // Saves the month

    for (auto &yr : Conc)         // Runs through all years
    {
      df << '\t';                 // Data spacer
      if (yr.second[m] > 0)       // Checks that data for the month is positive...
      {
        if (p.yDataType() != "pace")    // (Distance or Speed)
          df << yr.second[m];           // ...and if so, saves it in plain format or...
        else
          df << lobo::mmmssxxx(yr.second[m]);   // ...as a pace.
      }
    }

    df << endl;     // New line
  }

  df.close();
}
//----------------------------------------------------------------------
// Write only if data is not zero - double version
//----------------------------------------------------------------------
ofstream &WriteNonZero(ofstream & os, double d)
{
  os << '\t';

  if (d != 0)
    os << d;

  return os;  // Returns stream to allow WriteNonZero(os,d) << "something"
}
//----------------------------------------------------------------------
// Write only if data is not zero - unsigned version
//----------------------------------------------------------------------
ofstream &WriteNonZero(ofstream & os, unsigned d)
{
  os << '\t';

  if (d != 0)
    os << d;

  return os;  // Returns stream to allow WriteNonZero(os,d) << "something"
}
//----------------------------------------------------------------------
// Write time-distance scatter and calculate f(x) = a x regression
// Returns the value of a for running, swimming and cycling;
//----------------------------------------------------------------------
void WriteTDScatter(const std::string &fn, const cpPlot &p,
                      const fitness &fit,
                      double &ar, double &as, double &ac,
                      double &mr, double &ms, double &mc)
{
  double rxy{0.0}, rx2{0.0},    // Variable for run Sum xy and  Sum x2
         sxy{0.0}, sx2{0.0},    // Variable for swim Sum xy and  Sum x2
         cxy{0.0}, cx2{0.0};    // Variable for cycling Sum xy and  Sum x2

  // Vectors for data
  typedef pair<double,double> DT;
  vector<DT> R, S, C;

  // Runs through all exercises loading vectors
  // and calculating linear regression sums
  for ( auto &e : fit.Exercises() )
  {
    // Checks filter status
    bool IncludeThis = true;

    if (!p.Filter().empty())
    {
      auto it = p.Filter().begin();   // First element (may be the behavior flag)

      // Only continues if term was not yet found. Increments it before doing anything else
      while ( IncludeThis && (++it != p.Filter().end()) )
        IncludeThis = ( e.GetEquipment().find(*it) == string::npos );

      if (p.Filter()[0] == "+")     // Filter should include, instead of exclude terms
        IncludeThis = !IncludeThis; // Reverses filter action.
    }

    if (IncludeThis)
    {
      double distance = e.GetDistance(),
             duration = e.GetDuration();

      switch (e.GetExercise())
      {
        case al::run :
          R.push_back({distance,duration});
          rxy += distance * duration;
          rx2 += distance * distance;
          break;
        case al::swim :
          S.push_back({distance,duration});
          sxy += distance * duration;
          sx2 += distance * distance;
          break;
        case al::cycle :
          C.push_back({distance,duration});
          cxy += distance * duration;
          cx2 += distance * distance;
          break;
      }
    }
  }

  // Calculates linear regression coefficients
  ar = !R.empty() ? rxy / rx2 : -1;
  as = !S.empty() ? sxy / sx2 : -1;
  ac = !C.empty() ? cxy / cx2 : -1;

  // Find maxima
  auto Comp = [](DT p1, DT p2) { return p1.first < p2.first; } ;  // Lambda pair comparison

  if (!R.empty())
    mr = ( *max_element(R.begin(), R.end(), Comp) ).first * 1.1;
  if (!S.empty())
    ms = ( *max_element(S.begin(), S.end(), Comp) ).first * 1.1;
  if (!C.empty())
    mc = ( *max_element(C.begin(), C.end(), Comp) ).first * 1.1;

  ofstream df(fn);    // Opens data file

  if (p.Exercise() == al::all)    // Create a file with data from all exercises
  {

    // Looks for the size of the largest data set
    size_t largest = R.size() > S.size() ?
                      ( R.size() > C.size() ? R.size() : C.size() ) :
                      ( S.size() > C.size() ? S.size() : C.size() ) ;

    // Saves the data for all activities. The 3 data sets have different sizes
    // so one must check whether there is data to save.
    for (size_t i = 0; i < largest; i++)
    {
      if (i < R.size())  // Run data set still exists
        df << R[i].first << '\t' << lobo::hhmmss(R[i].second) << '\t';
      else                // No more run data...
        df << "\t\t";     // ...saves the data separators only.

      if (i < S.size())  // Swim data set still exists
        df << S[i].first << '\t' << lobo::hhmmss(S[i].second) << '\t';
      else                // No more swim data...
        df << "\t\t";     // ...saves the data separators only.

      if (i < C.size())  // Cycle data set still exists
        df << C[i].first << '\t' << lobo::hhmmss(C[i].second);
      else                // No more cycling data...
        df << "\t";       // ...saves the data separators only.

      df << endl;
    }
  }
  else                    // Only create one file with data for a single exercise
  {
    switch (p.Exercise())
    {
      case al::run :
        for (size_t i = 0; i < R.size(); i++)
          df << R[i].first << '\t' << lobo::hhmmss(R[i].second) << endl;
        break;

      case al::swim :
        for (size_t i = 0; i < S.size(); i++)
          df << S[i].first << '\t' << lobo::hhmmss(S[i].second) << endl;
        break;

      case al::cycle :
        for (size_t i = 0; i < C.size(); i++)
          df << C[i].first << '\t' << lobo::hhmmss(C[i].second) << endl;
        break;
    }
  }

  df.close();  // Vectors for data
}
//----------------------------------------------------------------------
// Write file with weekly comparative data
// [Distance, Pace, Speed] is accumulated in [Week, Month]
//----------------------------------------------------------------------
void WriteWeekData(const std::string &fn, const cpPlot &p,
                   const stats &st, double y1, double y2, bool IncLineNo)
{
  vector< pair<string,double> > OneYear;  // Creates a vector to hold Weely data
  OneYear.reserve(53);                    // Reserves places for at most 53 (Monday based) weeks

  map< int, vector< pair<string,double> > > Conc; // Table to store concomitant {year,week} data

  for (auto &yr : st.Year())          // Runs through all years
  {
    if (stats::Year(yr) >= y1 && stats::Year(yr) <= y2)   // Year is in the sought range
      Conc[stats::Year(yr)] = OneYear;                    // Inserts one full set of months per year
  }

  // Runs through all weeks
  for ( auto &wk : st.Week() )
  {
    if (stats::WeekYr(wk) >= y1 && stats::WeekYr(wk) <= y2)   // Year is in the sought range
    {
      // Initializes a reference according to the chosen exercise
      const stats::counters &ct = (p.Exercise() == al::run ?
                                                    stats::WeekCat(wk).Run :
                                                    p.Exercise() == al::cycle ?
                                                                     stats::WeekCat(wk).Cycle :
                                                                     stats::WeekCat(wk).Swim);
      // Loads the data according to Year and Week in Concomitant vector
      //
      // Distance
      //
      if (p.yDataType() == "distance" && ct.Distance > 0.)
      {
        //
        // Accumulated distance
        //
        if (p.Accumulate())
        {
          if (!Conc[stats::WeekYr(wk)].empty())     // This year have alreay weeks initialized
          {
            double prev = Conc[stats::WeekYr(wk)].back().second;    // Last value in vector
            Conc[stats::WeekYr(wk)].push_back( {lobo::DDMM(st.Week(wk)), prev + ct.Distance} );   // Adds current distance to previous
          }
          else                                      // First week with data this year
          {
            Conc[stats::WeekYr(wk)].push_back( {lobo::DDMM(st.Week(wk)), ct.Distance} );
          }
        }
        //
        // Normalized distance
        //
        else if (p.Normalize())
        {
          int n = ct.Events;
          if (n == 0) n = 1 ;
          Conc[stats::WeekYr(wk)].push_back( {lobo::DDMM(st.Week(wk)), ct.Distance / n} );
        }
        //
        // Plain distance
        //
        else
        {
          Conc[stats::WeekYr(wk)].push_back( {lobo::DDMM(st.Week(wk)), ct.Distance} );
        }
      }
      //
      // Pace
      //
      else if (p.yDataType() == "pace" && ct.Pace > 0.)
        Conc[stats::WeekYr(wk)].push_back( {lobo::DDMM(st.Week(wk)), ct.Pace} );
      //
      // Speed
      //
      else if (p.yDataType() == "speed" && ct.Speed > 0.)
        Conc[stats::WeekYr(wk)].push_back( {lobo::DDMM(st.Week(wk)), ct.Speed} );

    }
  }

  ofstream df(fn);  // Opens data file

  // Saves data in two columns for each year - There are at most 53 rows
  for (size_t w = 0; w < 53; w++)
  {
    if (IncLineNo)      // Start line with the line number
      df << w << '\t';

    for (auto &wd : Conc)
    {
      const vector< pair<string,double> > &v = wd.second;   // Alias for weekly vector

      string nlt = wd.first != y2 ? "\t" : "";    // Not a last tab -- avoids a dangling tab at the end of the line

      if (w < v.size())
      {
        df << v[w].first << '\t';

        if (p.yDataType() != "pace")            // Distance or Speed...
          df << v[w].second  << nlt;                   // ...saves it in plain format or...
        else
          df << lobo::mmmssxxx(v[w].second) << nlt;   // ...as a pace.
      }
      else
        df << '\t' << nlt;
    }

    df << endl;     // New line
  }

  df.close();
}
//----------------------------------------------------------------------
// Write file with yearly data
//----------------------------------------------------------------------
void WriteYearlyData(const string &fn, const cpPlot &p, const stats &st)
{
  ofstream df(fn);  // Opens data file

  // Runs through all years
  for ( auto &ct : st.Year() )
  {
    df << stats::Year(ct);      // Writes the year

    if (p.yDataType() == "distance")      // Yearly distances
    {
      if (p.Multi() & al::all_inc)              // Include total activities in year
      {
        double d = stats::YearCat(ct).All.Distance;   // Distance
        unsigned n = stats::YearCat(ct).All.Events;   // Number of events
        if ( n == 0 ) n = 1;                          // Avoids dvi by 0
        df << '\t' << (p.Normalize() ? d / n : d );   // Writes the data
      }

      if (p.Multi() & al::run)                  // Include running stats
      {
        double d = stats::YearCat(ct).Run.Distance;   // Distance
        unsigned n = stats::YearCat(ct).Run.Events;   // Number of events
        if ( n == 0 ) n = 1;                          // Avoids div by 0
        df << '\t' << (p.Normalize() ? d / n : d );   // Writes the data
      }

      if (p.Multi() & al::cycle)                // Include cycling stats
      {
        double d = stats::YearCat(ct).Cycle.Distance; // Distance
        unsigned n = stats::YearCat(ct).Cycle.Events; // Number of events
        if ( n == 0 ) n = 1;                          // Avoids div by 0
        df << '\t' << (p.Normalize() ? d / n : d );   // Writes the data
      }

      if (p.Multi() & al::swim)                 // Include swimming stats
      {
        double d = stats::YearCat(ct).Swim.Distance;  // Distance
        unsigned n = stats::YearCat(ct).Swim.Events;  // Number of events
        if ( n == 0 ) n = 1;                          // Avoids div by 0
        df << '\t' << (p.Normalize() ? d / n : d );   // Writes the data
      }
    }
    else if (p.yDataType() == "events")   // Yearly number of events
    {
      if (p.Multi() & al::all_inc)              // Include total activities in year
        df << '\t' << stats::YearCat(ct).All.Events;

      if (p.Multi() & al::run)                  // Include running stats
        df << '\t' << stats::YearCat(ct).Run.Events;

      if (p.Multi() & al::cycle)                // Include cycling stats
        df << '\t' << stats::YearCat(ct).Cycle.Events;

      if (p.Multi() & al::swim)                 // Include swimming stats
        df << '\t' << stats::YearCat(ct).Swim.Events;
    }

    df << endl;

  }
  df.close();
}
