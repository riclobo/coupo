//----------------------------------------------------------------------
// Couch potato general options struct
//----------------------------------------------------------------------
#include <algorithm>
//----------------------------------------------------------------------
#include "cpgenopt.h"
#include "exercise.h"
#include "trim.h"
//----------------------------------------------------------------------
// Locally defined function to convert argument into a no parsing list
//----------------------------------------------------------------------
void Ignore(const std::string &args, std::unordered_set<int> &us, bool &InvIg)
{
  if ( args.empty() ) return;   // Empty parameter list, nothing to do

  size_t p1 = 0,                // First string position
         p2 = args.find(",");   // Looks for first ',´

  while (p2 != std::string::npos)    // While a comma is found...
  {
    if (args.substr(p1,p2-p1) == "not" )  // 'not' argument found
    {
      InvIg = true;                       // Sets the parse instead of ignore flag
      p1 = p2 + 1;                        // Goes just past the previous ','
      p2 = args.find(',', p1);            // Looks for next ','
    }
    else                                  // Argument is supposed to be a plot id
    {
      us.insert( std::stoi(args.substr(p1, p2 - p1)) ); // Puts parameter in the list
      p1 = p2 + 1;                                      // Goes just past the previous ','
      p2 = args.find(',', p1);                          // Looks for next ','
    }
  }

  // The last parameter is not terminated by a comma
  us.insert( std::stoi(args.substr(p1)) );    // Puts parameter in the list
}
//----------------------------------------------------------------------
// Locally defined function to load one exericise type in one liners
//----------------------------------------------------------------------
void OneLinerExercise(unsigned ex, std::string &args, OneLiners &ol)
{
  ol.Show = true;           // Sets the 'show' this one liner flag

  if (args.empty()) return; // The job is done and Last_n, Fastest_n and Longest_n default to 10

  size_t p = args.find(',');                                // Looks for comma...
  ol.Last_n = std::stoi(args.substr(0,p));                  // ...and loads first parameter
  args = (p != std::string::npos) ? args.substr(p+1) : "";  // Trims first parameter out

  if (args.empty()) return;                   // No comma was found, so Fastest_n and Longest_n default to 10

  p = args.find(',');                         // Looks for comma...
  ol.Fastest_n = std::stoi(args.substr(0,p)); // ...and loads second parameter
  args = (p != std::string::npos) ? args.substr(p+1) : "";
  if (args.empty()) return;                   // No comma was found, so Longest_n default to 10

  ol.Longest_n = std::stoi(args); // Loads last parameter
}
//----------------------------------------------------------------------
// Locally defined function to load one liners definitions
//----------------------------------------------------------------------
void SetOneLiners(const std::string &args, OneLinerSummary &s)
{
  if ( args.empty() ||                                  // Empty parameter list or...
       std::count(args.begin(), args.end(), ',') > 3)   // ...too many parameters. Error
  {
    std::cout << "Error: OneLiner(): Takes from 1 to 4 arguments. Got '" << args << "'" << std::endl;
    exit(0);
  }

  unsigned ex = exercise::ExeKind(args[0]);   // Gets first letter of first argument and converts to exercise

  if (ex == al::nothing)
  {
    std::cout << "Error: OneLiner(): Unknown exercise. Got '" << args << "'" << std::endl;
    exit(0);
  }

  size_t p = args.find(",");              // Looks for first ',´
  std::string newargs = (p != std::string::npos) ? args.substr(p+1) : "";   // Erases first parameter

  // Loads proper one liner exercise
  switch (ex)
  {
    case al::run:   OneLinerExercise(ex, newargs, s.Run);   return;
    case al::swim:  OneLinerExercise(ex, newargs, s.Swim);  return;
    case al::cycle: OneLinerExercise(ex, newargs, s.Cycle); return;
  }
}
//----------------------------------------------------------------------
// Constructor from a queue<string> (cmd...value...cmd...value...)
//----------------------------------------------------------------------
cpGenOpt::cpGenOpt(clparser::strqueue &q)
{
  while (!q.empty())        // Goes trough General() queue
  {
    std::string s = q.front();    // Reads command and...
    q.pop();                      // ...removes it from the queue.

    if (s == "dontcallgp")        // Command was call gnuplot at end of calculations
    {
      DontCallgp = true;          // If the command is present the flag is true
      q.pop();                    // No parameter is expected just pop the queue.
    }
    else if (s == "input")        // Command was input file name
    {
      FileIn = lobo::reduce(q.front(), "", "\"");   // Reads next item (expects file name)...
      q.pop();                                      // ...and removes it from the queue.
    }
    else if (s == "fileprefix")   // Command was file prefix
    {
      FilePrefix = lobo::reduce(q.front(), "", "\""); // Reads next item (expects a string)...
      q.pop();                                        // ...and removes it from the queue.
    }
    else if (s == "gpterm")       // Command sets the gnuplot terminal
    {
      gpTerm = lobo::reduce(q.front(), "", "\"");   // Reads next item (expects a string)...
      q.pop();                                      // ...and removes it from the queue.
    }
    else if (s == "gptermopt")    // Command sets the gnuplot terminal options
    {
      gpTermOpt = lobo::reduce(q.front(), "", "\"");  // Reads next item (expects a string)...
      q.pop();                                        // ...and removes it from the queue.
    }
    else if (s == "longsize")     // Command sets the long edge of the page
    {
      PlotLongSize = stoi(q.front());               // Reads next item (expects a number)...
      q.pop();                                      // ...and removes it from the queue.
    }
    else if (s == "gpversion")     // Gnuplot version
    {
      Version = stoi(q.front());                    // Reads next item (expects a number)...
      q.pop();                                      // ...and removes it from the queue.
    }
    else if (s == "plottofile")   // Extention of file to save plots
    {
      FigFileExt = lobo::reduce(q.front(), "", "\""); // Reads next item (expects a string)...
      q.pop();                                        // ...and removes it from the queue.
    }
    else if (s == "report")       // Command was 'Generate a report'
    {
      Report = true;              // If the command is present the flag is true
      q.pop();                    // No parameter is expected just pop the queue.
    }
    else if (s == "oneliner")     // Include one liners in report
    {
      SetOneLiners(lobo::reduce(q.front(), "", "\""), Summary);
      q.pop();
    }
    else if (s == "ignore")      // Command was to ignore series of plots in this run
    {
      Ignore(lobo::reduce(q.front(), "", "\""), NoParsing, InvIg);  // Loads the no parsing list
      q.pop();                    // No parameter is expected just pop the queue.
    }
  }
}
