//----------------------------------------------------------------------
// Class definition for one fitness exercise
// (c) R.P.S.M. Lobo -- rpsml@yahoo.com
//----------------------------------------------------------------------
#include "exercise.h"
#include "datetime.h"
#include <iomanip>
#include <sstream>
//----------------------------------------------------------------------
// Constructor with the whole shebang
//----------------------------------------------------------------------
exercise::exercise(time_t TimeStamp,
                   unsigned Act,
                   const std::string &Equipment,
                   const std::string &Location,
                   const std::string &Note,
                   bool Competition,
                   double Dist,
                   unsigned Duration,
                   unsigned HRMAX, unsigned HRAVE,
                   double Weight,
                   bool hmsTime,
                   bool UnixTime) :
  hms_t(hmsTime),
  unix_t(UnixTime),
  compet(Competition),
  time_stamp(TimeStamp),
  duration(Duration),
  act(Act),
  equipment(Equipment),
  location(Location),
  note(Note),
  dist( int(Dist * 1000.) ),
  HRMax(HRMAX), HRAve(HRAVE),
  weight( int(Weight * 1000.) )
{
}
//----------------------------------------------------------------------
// Copy constructor
//----------------------------------------------------------------------
exercise::exercise(const exercise &a) :
  hms_t(a.hms_t),
  unix_t(a.unix_t),
  compet(a.compet),
  time_stamp(a.time_stamp),
  duration(a.duration),
  act(a.act),
  equipment(a.equipment),
  location(a.location),
  note(a.note),
  dist(a.dist),
  HRMax(a.HRMax), HRAve(a.HRAve),
  weight(a.weight)
{
}
//----------------------------------------------------------------------
// Set the unix time flag
//----------------------------------------------------------------------
void exercise::UseUnixTime(bool UnixTime)
{
  unix_t = UnixTime;
}
//----------------------------------------------------------------------
// (Un)set the hh:mm:ss flag
//----------------------------------------------------------------------
void exercise::TimeInSeconds(bool hmsTime)
{
  hms_t = hmsTime;
}
//----------------------------------------------------------------------
// Set the time stamp in time_f format
//----------------------------------------------------------------------
void exercise::SetTimeStamp(time_t TS)
{
	time_stamp = TS;
}
//----------------------------------------------------------------------
// Get the time stamp in time_t format
//----------------------------------------------------------------------
time_t exercise::GetTimeStamp() const
{
	return time_stamp;
}
//----------------------------------------------------------------------
// Set the exercise as competition
//----------------------------------------------------------------------
void exercise::SetCompetition(bool Comp)
{
  compet = Comp;
}
//----------------------------------------------------------------------
// Is the exercise a competition ?
//----------------------------------------------------------------------
bool exercise::GetCompetition() const
{
  return compet;
}
//----------------------------------------------------------------------
// Set the exercise duration
//----------------------------------------------------------------------
void exercise::SetDuration(unsigned Duration)
{
  duration = Duration;
}
//----------------------------------------------------------------------
// Get the exercise duration
//----------------------------------------------------------------------
unsigned exercise::GetDuration() const
{
  return duration;
}
//----------------------------------------------------------------------
// Set the exercise kind
//----------------------------------------------------------------------
void exercise::SetExercise(unsigned Act)
{
  act = Act;
}
//----------------------------------------------------------------------
// Get the exercise kind
//----------------------------------------------------------------------
unsigned exercise::GetExercise() const
{
  return act;
}
//----------------------------------------------------------------------
// Set the equipment used
//----------------------------------------------------------------------
void exercise::SetEquipment(const std::string &Equipment)
{
  equipment = Equipment;
}
//----------------------------------------------------------------------
// Get the equipment used
//----------------------------------------------------------------------
std::string exercise::GetEquipment() const
{
  return equipment;
}
//----------------------------------------------------------------------
// Set the exercise location
//----------------------------------------------------------------------
void exercise::SetLocation(const std::string &Location)
{
  location = Location;
}
//----------------------------------------------------------------------
// Get the exercise location
//----------------------------------------------------------------------
std::string exercise::GetLocation() const
{
  return location;
}
//----------------------------------------------------------------------
// Set the exercise location
//----------------------------------------------------------------------
void exercise::SetNote(const std::string &Note)
{
  note = Note;
}
//----------------------------------------------------------------------
// Get the exercise location
//----------------------------------------------------------------------
std::string exercise::GetNote() const
{
  return note;
}
//----------------------------------------------------------------------
// Set the distance (in km)
//----------------------------------------------------------------------
void exercise::SetDistance(double Dist)
{
  dist = int(Dist * 1000.);
}
//----------------------------------------------------------------------
// Get the distance (in km)
//----------------------------------------------------------------------
double exercise::GetDistance() const
{
  return ( double(dist) / 1000. );
}
//----------------------------------------------------------------------
// Set the weight (in kg)
//----------------------------------------------------------------------
void exercise::SetWeight(double W)
{
  weight = int(W * 1000.);
}
//----------------------------------------------------------------------
// Get the weight (in kg)
//----------------------------------------------------------------------
double exercise::GetWeight() const
{
  return ( double(weight) / 1000. );
}
//----------------------------------------------------------------------
// Set average heart rates
//----------------------------------------------------------------------
void exercise::SetAveHR(unsigned HRAVE)
{
  HRAve = HRAVE;
}
//----------------------------------------------------------------------
// Set maximum heart rate
//----------------------------------------------------------------------
void exercise::SetMaxHR(unsigned HRMAX)
{
  HRMax = HRMAX;
}
//----------------------------------------------------------------------
// Get average heart rates
//----------------------------------------------------------------------
unsigned exercise::GetAveHR() const
{
  return HRAve;
}
//----------------------------------------------------------------------
// Get maximum heart rates
//----------------------------------------------------------------------
unsigned exercise::GetMaxHR() const
{
  return HRMax;
}
//----------------------------------------------------------------------
// Heart rate / km
//----------------------------------------------------------------------
unsigned long exercise::BeatsKm() const
{
  return ( HRAve * Pace() );
}
//----------------------------------------------------------------------
// Pace in min/km
//----------------------------------------------------------------------
double exercise::Pace() const
{
  return ( 60.0 / Speed() );
}
//----------------------------------------------------------------------
// Speed in km/h
//----------------------------------------------------------------------
double exercise::Speed() const
{
  return ( 3.6 * double(dist) / double(duration) );
}
//----------------------------------------------------------------------
// Print a one line formated output
//----------------------------------------------------------------------
std::string exercise::OneLiner() const
{
  std::stringstream rv;

  rv << lobo::DDMMYYYY(GetTimeStamp()) << ": "
     << std::fixed << std::setprecision(3)
     << std::setw(7) << GetDistance() << " km in ";

  unsigned t = GetDuration();
  rv << std::setw(8);
  if ( t >= 3600 )
    rv << lobo::hhmmss(GetDuration());
  else
    rv << lobo::mmmss(GetDuration());

  rv << " at " << std::setw(7) << Speed() << " km/h (";

  rv << std::setw(8);
  if (GetExercise() != al::swim)
    rv << lobo::mmmssxxx(Pace()) << " min/km) in ";
  else
    rv << lobo::mmmssxxx(Pace() / 10.) << " min/100 m) in ";

  rv << GetLocation() << " " << GetNote();

  return rv.str();
}
//----------------------------------------------------------------------
// (Static) List of quantities in the saved order
//----------------------------------------------------------------------
std::string exercise::Header()
{
  return "Date\tKind\tEquipement\tLocation\tNote\tDistance\tDuration\tHR Ave\tHR Max\tWeight";
}
//----------------------------------------------------------------------
// (Static) Returns the exercise kind code
//----------------------------------------------------------------------
unsigned exercise::ExeKind(char c)
{
  if      (c == 'r') return al::run;
  else if (c == 'c') return al::cycle;
  else if (c == 's') return al::swim;
  else if (c == 'a') return al::all;
  else               return al::nothing;
}
//----------------------------------------------------------------------
// String with exercise description
//----------------------------------------------------------------------
std::string exercise::ExeName(unsigned e)
{
  switch (e)
  {
    case al::run:   return "Running";
    case al::cycle: return "Cycling";
    case al::swim:  return "Swimming";
    case al::all:   return "All";
    default:        return "";
  }
}
//----------------------------------------------------------------------
// Operator =
//----------------------------------------------------------------------
exercise &exercise::operator=(const exercise &rhs)
{
  if (this != &rhs)
  {
    hms_t = rhs.hms_t;
    unix_t = rhs.unix_t;
    compet = rhs.compet;
    time_stamp = rhs.time_stamp;
    duration = rhs.duration;
    act = rhs.act;
    location = rhs.location;
    dist = rhs.dist;
    HRMax = rhs.HRMax;
    HRAve = rhs.HRAve;
    weight = rhs.weight;
  }

  return *this;
}
//----------------------------------------------------------------------
// Operator <
//----------------------------------------------------------------------
bool exercise::operator<(const exercise& rhs) const
{
	return ( time_stamp < rhs.time_stamp );
}
//----------------------------------------------------------------------
// stream out
//----------------------------------------------------------------------
std::ostream &operator<<(std::ostream &os, const exercise& a)
{
  // Date
  if (a.unix_t)
    os << a.time_stamp << '\t';
  else
    os << lobo::DDMMYYYY(a.time_stamp) << '\t';

  os << a.act             << '\t' << a.equipment  << '\t'
     << a.location        << '\t' << a.note       << '\t'
     << size_t(a.compet)  << '\t' << a.dist       << '\t' ;

  // Duration
  if (a.hms_t)
  {
    if (a.duration < 60)          // Less than 60 s
      os << a.duration << '\t';
    else if (a.duration < 3600)   // Less than 1 h
      os << lobo::mmmss(a.duration) << '\t' ;
    else                          // At least 1 h
      os << lobo::hhmmss(a.duration) << '\t';
  }
  else
    os << a.duration << '\t';

  // Everything else
  os << a.HRAve << '\t' << a.HRMax << '\t' << a.weight;

  os << std::endl;			// Forces a new line at the end of output
  return os;
}
//----------------------------------------------------------------------
// Local function to parse one activty object from a line input
//----------------------------------------------------------------------
size_t GetEntry(const std::string &line, char c, std::string &s, size_t p1)
{
  size_t p = line.find(c, p1);  // Find first 'c' after position p1
  s = line.substr(p1, p - p1);  // Extract substring from p1 to p
  return p;                     // Return position of newly found 'c'
}
//----------------------------------------------------------------------
// stream in -- Consider tab separated values
//----------------------------------------------------------------------
std::istream &operator>>(std::istream &is, exercise &a)
{
  // Uses a string to read the whole line
  std::string line, s;
  std::getline(is, line);
  size_t pos1 = 0,
         pos2;

  // Check if first entry it is a time_t or a string
  pos1 = GetEntry(line, '\t', s, pos1) + 1;
  pos2 = s.find('/');               // Looks for a slash in the string
  if (pos2 != std::string::npos)    // Found a slash so date format
  {
    int d, m, y;
    size_t p1 = s.find_first_of('/'),
           p2 = s.find_last_of('/');
    d = stoi(s.substr(0, p1));
    m = stoi(s.substr(p1 + 1, p2 - p1));
    y = stoi(s.substr(p2 + 1));
    if (y < 100)
      y += (y < 50 ? 2000 : 1900);
    a.time_stamp = lobo::SetDate(d, m, y);
  }
  else                            // No slash means unix time
  {
    a.time_stamp = std::stoi(s);
  }

  // Read act -- Accepts a number code or a word run, running, cycle, ... (only checks the first letter)
  pos1 = GetEntry(line, '\t', s, pos1) + 1;
  if (s[0] == 'R' || s[0] == 'r')       // Run, run, running, etc
    a.act = al::run;
  else if (s[0] == 'C' || s[0] == 'c')  // Cycling, c, cycle, etc
    a.act = al::cycle;
  else if (s[0] == 'S' || s[0] == 's')  // Swim, S, etc
    a.act = al::swim;
  else if (s[0] == 'N' || s[0] == 'n')  // Nothing, nada, etc
    a.act = al::nothing;
  else                                  // Assumes it is a number
    a.act = std::stoi(s);

  // Read equipment
  pos1 = GetEntry(line, '\t', s, pos1) + 1;
  a.equipment = s;

  // Read location
  pos1 = GetEntry(line, '\t', s, pos1) + 1;
  a.location = s;

  // Read note
  pos1 = GetEntry(line, '\t', s, pos1) + 1;
  a.note = s;

  // Read compet
  pos1 = GetEntry(line, '\t', s, pos1) + 1;
  a.compet = bool(std::stoi(s));

  // Read distance
  pos1 = GetEntry(line, '\t', s, pos1) + 1;
  a.dist = std::stoi(s);

  // Check if second entry it is an unsigned or string
  pos1 = GetEntry(line, '\t', s, pos1) + 1;
  pos2 = s.find(':');               // Looks for a colon in the string
  if (pos2 != std::string::npos)    // Found a slash so date format
  {
    size_t p1 = s.find_first_of(':'),   // Looks for fist ':'
           p2 = s.find_last_of(':');    // Looks for last ':'

    if (p1 == p2)     // Just one colon, time is mm:ss
      a.duration = stoi(s.substr(0, p1)) * 60 +
                   stoi(s.substr(p2 + 1));
    else
      a.duration = stoi(s.substr(0, p1)) * 3600 +
                   stoi(s.substr(p1 + 1, p2 -1)) * 60 +
                   stoi(s.substr(p2 + 1));
  }
  else                // No colon means time in seconds
  {
    a.duration = std::stoi(s);
  }

  // Read HRAve
  pos1 = GetEntry(line, '\t', s, pos1) + 1;
  a.HRAve = std::stoi(s);

  // Read HRMax
  pos1 = GetEntry(line, '\t', s, pos1) + 1;
  a.HRMax = std::stoi(s);

  // Read weight
  GetEntry(line, '\t', s, pos1);
  a.weight = std::stoi(s);

	return is;
}





