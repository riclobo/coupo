//----------------------------------------------------------------------
// Class to generate a gnuplot graphic
//----------------------------------------------------------------------
#ifndef GNUPLOTTER_H
#define GNUPLOTTER_H
//----------------------------------------------------------------------
#include <iostream>
#include <utility>
#include <string>
#include <vector>
//----------------------------------------------------------------------
#include "aliases.h"
#include "clparser.h"
//----------------------------------------------------------------------
class gnuplotter
{
  public:
    struct axis
    {
      axis() { }                      // Empty constructor
      axis(char a) : ax(a) { }        // Constructor with an axis type
      axis(char a, bool r, bool s,    // Initializer constructor
           const std::string &t,      // Required to use { } initialization
           const std::string &rd,     // because the variables were given
           const std::string &wr) :   // default values.
        ax(a), Rotate(r),
        ShowTics(s), Title(t),
        ReadFmt(rd), WriteFmt(wr)
        { }

      char ax{'x'};                   // Axis type (x, y, z, y2, etc)

      bool Rotate{false},             // Rotate labels by 90 degrees
           ShowTics{true};            // Show or hide tics

      std::string Title{""},          // Title for the axis
                  ReadFmt{""},        // Data format for reading -- either nothing or some of the time formats
                  WriteFmt{"%g"};     // Format for writing data -- numeric (%g, %e, etc or date/time specifiers)
    };

    // Constructors:
    // The empty constructor just creates a basic placeholder
    // The second creates the command set based on strings placed in a
    // FIFO queue (command...value(s)...command...value(s)...)
    gnuplotter(axis X, axis Y, unsigned id, unsigned xtics, unsigned version) :
      xa(X), ya(Y), gpVersion(version), xticsint(xtics),
      PlotID ("p" + std::to_string(id) + "_")

      { }

    // Call this as many times as there are curves to add
    void AddCurve(const std::string &fn,    // Data file name
                  const std::string &color, // Line color (#RRGGBB)
                  const std::string &label, // Label for legend
                  int colx = 1,             // x and ...
                  int coly = 2,             // ...y columns in data file
                  bool lines = true,        // Plot with lines
                  bool symbols = false,     // Plot with symbols (points)
                  bool thick = false,       // Use thick line
                  bool smooth = false);     // Smooth out the data

    // Call this as many times as there are functions to add
    void AddFunc(const std::string &fn,    // function to plot
                 const std::string &color, // Line color (#RRGGBB)
                 const std::string &label, // Label for legend
                 bool lines = true,        // Plot with lines
                 bool symbols = false,     // Plot with symbols (points)
                 bool thick = false);      // Use thick line

    // Create the plot and return a string with the commands
    const std::string &CreatePlot(const std::string &title);

    // Reference to plotting commands string
    const std::string &PlotCmd() const { return gpcmd; }

    // Each plot can have one, and only one histogram. The last one set is kept.
    void SetHistogram(const std::string &fn,    // Data file name
                      const std::string &color, // Color (#RRGGBB) for histogram
                      int scale = 1,            // Scale for histogram bars
                      bool NumericX = true);    // Labels are numeric (oposed to text)

    void SetClusterHgram(const std::string &fn,                     // Data file name
                         const std::vector< std::pair<std::string,std::string> > &data); // {Caption,Color} for each plot

    // Places the legend accordind to or'ed values -- see aliases.h
    void SetLegendPosition(unsigned pos);

    // Set range. There are version for numbers or for date and time -- see aliases.h for code
    void SetRange(const gnuplotter::axis &a, int code, double low, double high);
    void SetRange(const gnuplotter::axis &a, int code, const std::string &date1, const std::string &date2);

    // Set separator character for gnuplot data reading
    void SetSeparator(char sep) { datasep = sep; }

    // Shows a grid in the plot -- see aliases.h
    void ShowGrid(int Code,                     // Or'ed values for major and minor tics
                  const std::string &Mcolor,    // Major grid color
                  const std::string &mcolor);   // Minor grid color

    // Save gnuplot instructions to file
    bool Save(const std::string &FileName);

    // Friend output stream
    friend std::ostream &operator<<(std::ostream &os, const gnuplotter& gp);

  private:
    axis xa, ya;

    char datasep{'\t'};           // Data file separator

    std::string gpcmd{""};        // String containing the gnuplot commands

    unsigned CurrentPlot{0},      // Number of line plots
             gpVersion,           // Gnuplot version (4 or 5 and higher?)
             xticsint;            // Interval between x tics

    std::string PlotID;           // ID utilized in making gnupolot variables unique

    std::vector<std::string> Frame,    // Range, grid, legend, etc
                             PlotDefs, // Plot definitions
                             Plots,    // Plot command
                             Styles;   // Styles

};

#endif // GNUPLOTTER_H
