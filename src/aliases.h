//----------------------------------------------------------------------
// Aliases for numerical constants
//----------------------------------------------------------------------
#ifndef ALIASES_H
#define ALIASES_H
//----------------------------------------------------------------------
namespace al
{
  // Exercises
  const unsigned nothing = 0x00;
  const unsigned run     = 0x01;
  const unsigned cycle   = 0x02;
  const unsigned swim    = 0x04;
  const unsigned all_inc = 0x08;
  const unsigned all     = run | cycle | swim;

  // Recurring years
  const unsigned allyr = 0;

  // xtics
  const unsigned autofreq = 0;

  // Legend position
  const unsigned top    = 0x01;
  const unsigned bottom = 0x08;
  const unsigned right  = 0x02;
  const unsigned left   = 0x04;

  // Heart rate
  const unsigned ave  = 0x01;
  const unsigned max  = 0x02;
  const unsigned both = 0x03;

  // Y axis autoscae
  const unsigned autosc    = 0;
  const unsigned zerostart = 1;

  // Grid
  const unsigned MX = 0x01;
  const unsigned mx = 0x02;
  const unsigned MY = 0x04;
  const unsigned my = 0x08;
  const unsigned fullgr = MX | mx | MY |my;

  // Set range
  // can also use autosc defined above
  const unsigned low  = 0x01;
  const unsigned high = 0x02;

  // Time constants
  const unsigned onedy = 60 * 60 * 24;  // Seconds in one day
  const unsigned oneyr = onedy * 365;   // Seconds in one day

}
//----------------------------------------------------------------------
#endif // ALIASES_H
