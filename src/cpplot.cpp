//----------------------------------------------------------------------
// Class to parse a coupo queue into data for a plot
//----------------------------------------------------------------------
#include <algorithm>
//----------------------------------------------------------------------
#include "cpplot.h"
#include "exercise.h"
#include "trim.h"
//----------------------------------------------------------------------
// Constructor taking a queue<string>
//----------------------------------------------------------------------
cpPlot::cpPlot(clparser::strqueue &q)
{
  while (!q.empty())        // Goes trough General() queue
  {
    std::string s = q.front();   // Reads command and...
    q.pop();                     // ...removes it from the queue.

    //
    // Plot type commands
    //
    if (s == "compare")           // Compare plots
    {
      DefComp(lobo::reduce(q.front(), "", "\""));   // Defines compare plot - Expects optional data binning size
      q.pop();                    // Removes arguments list from the queue.
    }
    else if (s == "daily")        // Daily plots
    {
      DefDaily(lobo::reduce(q.front(), "", "\""));  // Defines daily plot - Expects optional data type
      q.pop();                    // Removes arguments list from the queue.
    }
    else if (s == "equipment")    // Equipment - Distance plot
    {
      DefEquip(lobo::reduce(q.front(), "", "\""));  // Defines equipement - expects no parameters
      q.pop();                    // Just pops next line from the queue
    }
    else if (s == "timedistance") // Time-Distance plot
    {
      DefTD(lobo::reduce(q.front(), "", "\""));     // Defines Time-Distance - expects no parameters
      q.pop();                    // Just pops next line from the queue
    }
    else if (s == "yearly")       // Yearly cumulators
    {
      DefYearly(lobo::reduce(q.front(), "", "\"")); // Defines daily plot - expects optional data type
      q.pop();                    // Just pops next line from the queue
    }
    //
    // Modifiers commands
    //
    else if (s == "accdist")      // Accumulated Distance
    {
      Acc = true;     // Sets the accumulated distance flag
      q.pop();        // Just pops next line from the queue
    }
    else if (s == "exercise")     // Sets the exercise
    {
      SetExe(lobo::reduce(q.front(), "", "\""));  // Parses arguments list
      q.pop();                                    // Removes it from the queue
    }
    else if (s == "filter")       // Loads filter list
    {
      SetFilters(lobo::reduce(q.front(), "", "\""));  // Parses arguments list
      q.pop();                                        // Removes it from the queue
    }
    else if (s == "heart")        // Which heart rates to plot
    {
      SetHR(lobo::reduce(q.front(), "", "\""));   // Parses arguments list
      q.pop();                                    // Removes it from the queue
    }
    else if (s == "highlight")    // Number of years to highlight in compare plots
    {
      SetHighlight(lobo::reduce(q.front(), "", "\""));  // Parses arguments list
      q.pop();                                    // Removes it from the queue
    }
    else if (s == "multi")        // Shows multiple exercises
    {
      SetMulti(lobo::reduce(q.front(), "", "\""));  // Parses arguments list
      q.pop();                                      // Removes it from the queue
    }
    else if (s == "normdist")     // Normalize distance by number of events
    {
      norm = true;    // Sets the normalization flag
      q.pop();        // Just pops next line from the queue
    }
    else if (s == "range")        // Defines the range to plot
    {
      SetRange(lobo::reduce(q.front(), "", "\""));  // Parses arguments list
      q.pop();                                      // Removes it from the queue
    }
    else if (s == "split")
    {
      split = true;  // Sets the split flag
      q.pop();       // Just pops next line from the queue
    }
    else if (s == "smooth")       // Smooth out the data
    {
      smooth = true;  // Sets the smoothing flag
      q.pop();        // Just pops next line from the queue
    }
    else if (s == "title")       // Smooth out the data
    {
      title = lobo::reduce(q.front(), "", "\"");  // Sets the title
      q.pop();                                    // Just pops next line from the queue
    }
    //
    // Aesthetics commands
    //
    else if (s == "grid")         // Shows the grid
    {
      shgrd = true;     // Sets the show grid flag
      q.pop();          // Just pops next line from the queue
    }
    else if (s == "legend")       // Sets the legend position
    {
      SetLegend(lobo::reduce(q.front(), "", "\""));   // Parses arguments list
      q.pop();                                        // Removes it from the queue
    }
    else if (s == "showpts")      // Bypasses other decisions and shows points
    {
      frcpt = true;     // Sets the show points flag
      q.pop();          // Just pops next line from the queue
    }
    else if (s == "xtics")        // Sets the interval of major xtics
    {
      SetXTicInt(lobo::reduce(q.front(), "", "\""));    // Parses arguments list
      q.pop();                                          // Removes it from the queue
    }
    else if (s == "yscale")       // Defines how to handle the y scale
    {
      SetYScale(lobo::reduce(q.front(), "", "\""));     // Parses arguments list
      q.pop();                                          // Removes it from the queue
    }
    else        // Unknown command
    {
      errors += s + "() is not a known command in Plot() sections\n";   // Error message
      q.pop();                                          // Just pops next line from the queue
    }
  }
}
//----------------------------------------------------------------------
// Sets default values for Compare plots. The following are kept at
// their declaring values: Acc{false}, norm{false}, shgrd{false},
// exercise{al::run}, nhl{0}, legpos{al::top|al::right},
// yauto{al::zerostart}, range_i{""}, range_f{""},
//----------------------------------------------------------------------
void cpPlot::DefComp(clparser::csr s)
{
  if (s.empty() ||                                // Empty string...
      std::count(s.begin(), s.end(), ',') != 1 )  // ...or anything other than 2 args mean error...
  {
    std::cout << "Compare() requires two paramters (bin,data). Got '" + s   + "'\n";
    return;
  }

  size_t p = s.find(',');     // Looks for the comma

  // Checks if bin size is acceptable
  std::string r = s.substr(0, p);    // First parameter -- bin size

  if ( r == "week" || r == "month")
    xtype = r;
  else
    errors += "Compare(): unknown bin size: '" + r + "'\n";

  // Checks if data type is acceptable
  r = s.substr(p + 1);      // Second parameter -- data type

  if ( r == "distance" || r == "pace" || r == "speed")
    ytype = r;
  else
    errors += "Compare(): unknown data type: '" + r + "'\n";
}
//----------------------------------------------------------------------
// Sets default values for Daily plots. The following are kept at
// their declaring values: Acc{false}, shgrd{false},  exercise{al::run},
// hr{al::both}, legpos{al::top|al::right}, xticint{al::autofreq},
// yauto{al::zerostart}, range_i{""}, range_f{""}
//----------------------------------------------------------------------
void cpPlot::DefDaily(clparser::csr s)
{
  xtype = "day";

  if (s.empty())      // Empty string...
    return;           // ...keep default ytype{"Distance"} value

  // Checks if data is acceptable for daily plote
  if ( s == "distance" || s == "pace"    || s == "speed" ||
       s == "heart"    || s == "weight"  || s == "time")
    ytype = s;
  else
    errors += "Daily(): unknown y data type '" + s   + "'\n";
}
//----------------------------------------------------------------------
// Sets default values for Equipement plot. The following are kept at
// their declaring values: norm{false}, shgrd{false}
//----------------------------------------------------------------------
void cpPlot::DefEquip(clparser::csr s)
{
  if (!s.empty())     // Arguments is not an empty string: error...
  {
    errors += "Equipment() takes no argument. Got '" + s + "'\n";
    return;
  }

  // Defines pertinent quantities
  xtype = "equipment";
  ytype = "distance";
  exercise = al::all;
}
//----------------------------------------------------------------------
// Sets default values for Time-Distance plot. The following are kept at
// their declaring values: shgrd{false}
//----------------------------------------------------------------------
void cpPlot::DefTD(clparser::csr s)
{
  if (!s.empty())     // Arguments is not an empty string: error...
  {
    errors += "TimeDistance() takes no argument. Got '" + s + "'\n";
    return;
  }

  // Defines pertinent quantities
  xtype = "distance";
  ytype = "time";
  exercise = al::all;
  legpos = al::top | al::left;
}
//----------------------------------------------------------------------
// Sets default values for Yearly plots. The following are kept at
// their declaring values: norm{false}, shgrd{false}, multi{al::run},
// ytype{"distance"}, range_i{""},  range_f{""}, yauto{al::zerostart}
//----------------------------------------------------------------------
void cpPlot::DefYearly(clparser::csr s)
{
  xtype = "year";

  if (s.empty())      // Empty string...
    return;           // ...keep default ytype{"distance"} value

  // Checks if data is acceptable for daily plote
  if ( s != "distance" || s != "events")
    ytype = s;
  else
    errors += "Yearly(): unknown y data type '" + s   + "'\n";
}
//----------------------------------------------------------------------
// Sets the exercise type -- Expects optional exercise kind
//----------------------------------------------------------------------
void cpPlot::SetExe(clparser::csr s)
{
  if (s.empty())      // Empty string...
    return;           // ...keep default value

  // Checks if parameter is a known exercise type
  unsigned e = exercise::ExeKind(s[0]);
  if (e != al::nothing)
    exercise = e;
  else
    errors += "Exercise: unknown exercise type '" + s + "'\n";
}
//----------------------------------------------------------------------
// Loads the filter list -- Words in this list modify the data plotted.
// Its breadth varies in each plot type.
//----------------------------------------------------------------------
void cpPlot::SetFilters(clparser::csr s)
{
  if (s.empty())      // Empty string...
    return;           // ...nothing to set

  size_t p1 = 0,             // First string position
         p2 = s.find(",");   // Looks for first ',´

  while (p2 != std::string::npos)    // While a comma is found...
  {
    filter.push_back( s.substr(p1, p2 - p1) );  // Puts word in the list
    p1 = p2 + 1;                                // Goes just past the previous ','
    p2 = s.find(',', p1);                       // Looks for next ','
  }

  // The last parameter is not terminated by a comma
  filter.push_back( s.substr(p1) );  // Puts word in the list

  // Checks first filter term -- reserved for filter behavior
  if (filter[0] != "+" && filter[0] != "-")     // No behavior code is set
    filter.insert(filter.begin(),"-");          // Inserts the default behavior
}
//----------------------------------------------------------------------
// Sets the heart rate
//----------------------------------------------------------------------
void cpPlot::SetHR(clparser::csr s)
{
  if (s.empty())      // Empty string...
    return;           // ...keeps default hr{al::both} value

  if (s == "ave") hr = al::ave;
  else if (s == "max") hr = al::max;
  else if (s == "both") hr = al::both;
  else errors += "Heart(): unknown heart rate type '" + s + "'\n";
}
//----------------------------------------------------------------------
// Sets the number of years in highlight
//----------------------------------------------------------------------
void cpPlot::SetHighlight(clparser::csr s)
{
  if (s.empty())      // Empty string...
    return;           // ...keeps default nhl{0} value

  nhl = std::stoi(s);

  if (nhl < 0)
    errors += "Highlight(): number of years must be positive. Got '" + s + "'\n";
}
//----------------------------------------------------------------------
// Sets the mulitple exercises plot. Requires from 1 to 4 arguments
// among Run, Cycle, Swim, All.
//----------------------------------------------------------------------
void cpPlot::SetMulti(clparser::csr s)
{
  if ( s.empty() )      // Empty parameter list...
    return;             // ...keeps default multi{al::run} value.

  if (std::count(s.begin(), s.end(), ',') > 3)   // Too many parameters. Error
  {
    errors += "Multi(): Takes a maximum of 4 arguments. Got '" + s + "'\n";
    return;
  }

  multi = 0;

  size_t p = 0;   // First string position

  while (p != std::string::npos)    // While a comma is found...
  {
    // Gets the exercise code from first letter.
    // (p > 0 ? p + 1 : p) handles the case of the first index
    // as we cannot start with p = -1 (the character previours
    // to the first).
    unsigned e = exercise::ExeKind(s[(p > 0 ? p + 1 : p)]);

    if ( e == al::all )     // Ored all exercises value...
      e = al::all_inc;      // ...is converted to all included value.

    if ( e == al::nothing ) // Unknown exercise -- Error
      errors += "Multi(): Unknown argument in '" + s + "'\n";

    multi |= e;             // Adds value to multi flag
    p = s.find(',', p + 1); // Looks for next ','
  }
}
//----------------------------------------------------------------------
// Defines initial and final dates.
//----------------------------------------------------------------------
void cpPlot::SetRange(clparser::csr s)
{
  if (s.empty())    // Empty parameter list,
    return;         // ...keeps default range_i{""}, range_f{""} values

  size_t p = s.find(',');           // Looks for comma
  range_i = s.substr(0, p);         // Stores parameter as initial range

  if (p == std::string::npos)       // s had a single parameter...
    return;                         // ...keeps defaults for final range

  // s had two parameters, stores the remaining into final range
  range_f = s.substr(p + 1);
}
//----------------------------------------------------------------------
// Defines the legend position. Looks for {t or b} and {r or l}. All
// values may be present: t superseeds b and r superseeds l.
//----------------------------------------------------------------------
void cpPlot::SetLegend(clparser::csr s)
{
  if (s.empty())      // Empty string...
    return;           // ...keeps default legpos{al::top|al::right} value

  // Checks if argument list has exactly two characters
  if (s.length() != 2)
  {
    errors += "Legend(): argument must be two characters {t or b} and {r or l}. Got '" + s + "'\n";
    return;
  }

  legpos = 0;

  // Looks for 't' and 'b'
  size_t p = s.find('t'),
         q = s.find('b');

  // Sets 't' and 'b' in legpos or creates an error message
  if (p != std::string::npos) legpos |= al::top;
  else if (q != std::string::npos) legpos |= al::bottom;
  else errors += "Legend(): t or b missing in '" + s + "'\n";

  // Looks for 'r' and 's'
  p = s.find('r');
  q = s.find('l');

  // Sets 't' and 'b' in legpos or creates an error message
  if (p != std::string::npos) legpos |= al::right;
  else if (q != std::string::npos) legpos |= al::left;
  else errors += "Legend(): r or l missing in '" + s + "'\n";
}
//----------------------------------------------------------------------
// Sets the interval of major xtics
//----------------------------------------------------------------------
void cpPlot::SetXTicInt(clparser::csr s)
{
  if (s.empty())      // Empty string...
    return;           // ...keeps default xticint{al::autofreq} value

  std::string q = s ;

  // Deals with week and year aliases for yearly plots
  if (xtype == "year")
  {
    // Checks if 'week' is passed as a time alias -- Shows error if so
    size_t p1 = q.find("week");
    if (p1 != std::string::npos)
      errors += "Cannot use 'week' in 'Yearly()' plots\n";

    // Checks if year is passed and erases it if so (because year = 1)
    p1 = q.find("year");
    if (p1 != std::string::npos)
      q.erase(p1,4);

    // Checks if year was multiplied by a number and erases the * sign
    p1 = q.find("*");
    if (p1 != std::string::npos)
      q.erase(p1,1);

    // If the string is empty, it is because it was 'year'. Sets it to 1.
    if (q.empty())
      q = "1";
  }

  size_t p = q.find('*');   // Looks for multiplication sign

  if (p == std::string::npos)   // Did not find '*'
  {
    if      (q == "day")  xticint = al::onedy;
    else if (q == "year") xticint = al::oneyr;
    else                  xticint = std::stoi(q);   // Assumes it is a number
  }
  else                          // Found '*'
  {
    xticint = std::stoi( q.substr(0,p) );         // Assumes a number before *
    std::string r = lobo::trim( q.substr(p+1) );  // Gets string after '*'

    if      (r == "day")  xticint *= al::onedy;
    else if (r == "year") xticint *= al::oneyr;
    else    errors += "XTics(): cannot decipher parameter '" + s + "'\n";
  }
}
//----------------------------------------------------------------------
// Defines how to handle the y scale
//----------------------------------------------------------------------
void cpPlot::SetYScale(clparser::csr s)
{
  if (s.empty())      // Empty string...
    return;           // ...keeps default yauto{al::zerostart} value

  if      (s == "auto") yauto = al::autosc;
  else if (s == "zero") yauto = al::zerostart;
  else    errors += "YScale(): unknown scaling method '" + s + "'\n";
}
