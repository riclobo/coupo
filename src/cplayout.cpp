//----------------------------------------------------------------------
// struct to parse a coupo layout queue
//----------------------------------------------------------------------
#include "cplayout.h"
#include "trim.h"
//----------------------------------------------------------------------
// Constructor with a layout queue
//----------------------------------------------------------------------
cpLayout::cpLayout( clparser::strqueue &q )
{
  bool lscape = true,       // The default is landscape orientation
       shdate = false;      // The default show date state

  while (!q.empty())
  {
    std::string s = q.front();  // Reads command and...
    q.pop();                    // ...removes it from the queue.

    if (s == "addsingleplot")     // Add a plot to the single plot list
    {
      std::string r = lobo::reduce(q.front(), "", "\"");    // Reduces the string to the minimum
      if (!r.empty())
        Single.push_back( {lscape, shdate, std::stoi(r)} );
      q.pop();                      // No parameter is expected just pop the queue.
    }
    else if (s == "addrowplot")   // Add a list of plots to the plot-in-a-row list
    {
      AddGridPlot('r', lobo::reduce(q.front(), "", "\""), lscape, shdate);  // Parses the list...
      q.pop();                                                // ...and removes it from the queue.
    }
    else if (s == "addcolplot")   // Add a list of plots to the plot-in-a-column list
    {
      AddGridPlot('c', lobo::reduce(q.front(), "", "\""), lscape, shdate);   // Parses the list...
      q.pop();                                                // ...and removes it from the queue.
    }
    else if (s == "addgridplot")  // Add a list of plots to the plot-in-a-grid list
    {
      AddGridPlot('g', lobo::reduce(q.front(), "", "\""), lscape, shdate);  // Parses the list...
      q.pop();                                               // ...and removes it from the queue.
    }
    else if (s == "landscape")    // Change the landscape flag
    {
      lscape = true;              // Makes it true
      q.pop();                    // No parameter is expected
    }
    else if (s == "portrait")     // Change the landscape flag
    {
      lscape = false;             // Makes it false
      q.pop();                    // No parameter is expected
    }
    else if (s == "hidedate")     // Change the showdate flag
    {
      shdate = false;             // Makes it false
      q.pop();                    // No parameter is expected
    }
    else if (s == "showdate")     // Change the showdate flag
    {
      shdate = true;              // Makes it true
      q.pop();                    // No parameter is expected
    }
  }
}
//----------------------------------------------------------------------
// Add a 'r'ow, 'c'olumn or 'g'rid layout
//----------------------------------------------------------------------
void cpLayout::AddGridPlot(char c, const std::string &pars, bool lscape, bool shdate)
{
  if ( pars.empty() ) return;      // Empty parameter list, nothing to do

  // Starts a new list entry
  Grid.push_back( {lscape, shdate, 0, 0, std::vector<int>()} );

  size_t p1 = 0,             // First string position
         p2 = pars.find(",");   // Looks for first ',´

  while (p2 != std::string::npos)    // While a comma is found...
  {
    Grid.back().Plots.push_back( std::stoi(pars.substr(p1, p2 - p1)) ); // Puts parameter in the list
    p1 = p2 + 1;                                                        // Goes just past the previous ','
    p2 = pars.find(',', p1);                                            // Looks for next ','
  }

  // The last parameter is not terminated by a comma
  Grid.back().Plots.push_back( std::stoi(pars.substr(p1)) );            // Puts parameter in the list

  // Determines the number of rows and columns
  switch (c)
  {
    case 'r':
      Grid.back().nr = 1;
      Grid.back().nc = Grid.back().Plots.size();
      break;

    case 'c':
      Grid.back().nr = Grid.back().Plots.size();
      Grid.back().nc = 1;
      break;

    case 'g':
    default:
      int rows, cols;

      // Decides what is the best distributiuon of columns and rows
      int k = Grid.back().Plots.size();
      if ( k >= 1 && k <= 3 )
      {
        cols = 3;
        rows = 1;
      }
      else if ( k == 4 )
      {
        rows = cols = 2;
      }
      else if ( k == 5 || k == 6 )
      {
        cols = 3;
        rows = 2;
      }
      else if ( k >= 7 && k <= 9 )
      {
        cols = 3;
        rows = 3;
      }
      else if ( k >= 10 && k <= 12 )
      {
        cols = 4;
        rows = 3;
      }
      else
      {
        cols = 4;
        rows = k / cols;
        if (k % cols) rows++;
      }

      // Set nr and nc according to landscape orientation
      Grid.back().nc = ( lscape ? cols : rows );
      Grid.back().nr = ( lscape ? rows : cols );
  }
}
