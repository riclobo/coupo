//----------------------------------------------------------------------
// Class to parse a coupo queue into data for a plot
//----------------------------------------------------------------------
#ifndef CPPLOT_H
#define CPPLOT_H
//----------------------------------------------------------------------
#include <string>
//----------------------------------------------------------------------
#include "aliases.h"
#include "clparser.h"
//----------------------------------------------------------------------
class cpPlot
{
  public:
    cpPlot() { }       // Do nothing constructor -- useful for maps, vectors, etc.
    cpPlot(clparser::strqueue &q);

    clparser::csr Errors()  const { return errors; }

    unsigned Exercise()  const { return exercise; }

    clparser::csr xDataType() const { return xtype; }
    clparser::csr yDataType() const { return ytype; }

    clparser::csr Range_i()   const { return range_i; }
    clparser::csr Range_f()   const { return range_f; }

    unsigned Highlight() const { return nhl; }

    unsigned WhichHR()   const { return hr; }
    unsigned XTicInt()   const { return xticint; }
    unsigned YAuto()     const { return yauto; }
    unsigned LegendPos() const { return legpos; }
    unsigned Multi()     const { return multi; }

    bool Accumulate()   const { return Acc; }
    bool Normalize()    const { return norm; }
    bool ShowGrid()     const { return shgrd; }
    bool ShowPoints()   const { return frcpt; }
    bool Split()        const { return split; }
    bool Smooth()       const { return smooth; }
    std::string Title() const { return title; }

    const std::vector<std::string> &Filter() const { return filter; }

  private:
    void DefComp(clparser::csr s);
    void DefDaily(clparser::csr s);
    void DefEquip(clparser::csr s);
    void DefTD(clparser::csr s);
    void DefYearly(clparser::csr s);

    void SetExe(clparser::csr s);
    void SetFilters(clparser::csr s);
    void SetHR(clparser::csr s);
    void SetHighlight(clparser::csr s);
    void SetMulti(clparser::csr s);
    void SetRange(clparser::csr s);

    void SetLegend(clparser::csr s);
    void SetXTicInt(clparser::csr s);
    void SetYScale(clparser::csr s);

    bool        Acc{false},           // When true, shows the accumulated value up to each date
                frcpt{false},         // Force showing points
                norm{false},          // Normalize distance by number of events
                shgrd{false},         // Flag for showing the grid
                split{false},         // Flag to split filter key words in different curves
                smooth{false};        // Flag for superposing a smoothed out version of the curve

    unsigned    exercise{al::run},    // run, cycle, swim, all
                hr{al::both},         // Heart rate to show
                multi{al::run},       // Mulitple (or'ed) exercises
                nhl{0},               // Number of years, from last, to highlight
                legpos{al::top |
                       al::right},    // Position of the legend
                xticint{al::autofreq},// Spacing between major xtics
                yauto{al::zerostart}; // 0x00 is autoscale, 0x01 forces a start at 0

    std::string xtype{"day"},         // Day, Week, Month, Year, Equipment, Kind, Location, Obs
                ytype{"distance"},    // Distance, Pace, Speed, HRAve, Weight
                range_i{""},          // ( "" meas the largest possible ) Initial and...
                range_f{""},          // ...final values for range plotted.
                title{""},            // Title for the plot
                errors{""};           // Eventual parsing error messages

    std::vector<std::string> filter;  // Words to be filtered
};
#endif // CPPLOT_H
