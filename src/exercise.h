//----------------------------------------------------------------------
// Class definition for one fitness exercise
// (c) R.P.S.M. Lobo -- rpsml@yahoo.com
//----------------------------------------------------------------------
#ifndef EXERCISE_H
#define EXERCISE_H
//----------------------------------------------------------------------
#include "aliases.h"
//----------------------------------------------------------------------
#include <string>
#include <ctime>
#include <iostream>
//----------------------------------------------------------------------
class exercise
{
  public:
    // Empty constructor
    exercise(bool UnixTime = false) : unix_t(UnixTime) {}

    // Constructor with the whole shebang
    exercise(time_t TimeStamp,
             unsigned Act,
             const std::string &Equipment,
             const std::string &Location,
             const std::string &Note,
             bool Competition,
             double Dist,
             unsigned Duration,
             unsigned HRMAX, unsigned HRAVE,
             double Weight,
             bool hmsTime = true,
             bool UnixTime = false);

    // Copy constructor
    exercise(const exercise &a);

    ~exercise() {}  // Destructor

    // Setters and getters
    void UseUnixTime(bool UnixTime = true);
    void TimeInSeconds(bool hmsTime = false);

    void SetTimeStamp(time_t TS);
    time_t GetTimeStamp() const;

    void SetCompetition(bool Comp);
    bool GetCompetition() const;

    void SetDuration(unsigned Duration);
    unsigned GetDuration() const;

    void SetExercise(unsigned Act);
    unsigned GetExercise() const;

    void SetEquipment(const std::string &Equipment);
    std::string GetEquipment() const;

    void SetLocation(const std::string &Location);
    std::string GetLocation() const;

    void SetNote(const std::string &Note);
    std::string GetNote() const;

    void SetDistance(double Dist);  // km
    double GetDistance() const;     // km

    void SetWeight(double W);       // kg
    double GetWeight() const;       // kg

    void SetAveHR(unsigned HRAVE);
    void SetMaxHR(unsigned HRMAX);
    unsigned GetAveHR() const;
    unsigned GetMaxHR() const;

    // Useful functions

    unsigned long BeatsKm() const;      // Heart rate / km
    double Pace() const;                // min/km
    double Speed() const;               // Speed km/h

    std::string OneLiner() const;             // Print a one line formated output

    static std::string  Header();  // List of quantities in the saved order
    static unsigned     ExeKind(char c);
    static std::string  ExeName(unsigned e);

    // Operators
    exercise &operator=(const exercise &rhs);
    bool operator<(const exercise& rhs) const;

    // Stream operators
    friend std::ostream &operator<<(std::ostream &os, const exercise& a);
		friend std::istream &operator>>(std::istream &is, exercise &a);

  private:
    bool          hms_t,            // Save time in hh:mm:ss or seconds only
                  unix_t,           // Save date in Unix time or calendar
                  compet;           // The exercise was during a competition

    time_t        time_stamp = 0;    // Date and time of activity start
    unsigned      duration = 0;      // Duration of activity in seconds
    unsigned      act = al::nothing; // Exercise kind
    std::string   equipment = "",    // Equipment used
                  location = "",     // Location of activity
                  note = "";         // Any particular note about the activity

    unsigned dist = 0,    // Distance (in m)
             HRMax = 0,   // Maximum heart rate
             HRAve = 0,   // Average heart rate
             weight = 0;  // Weight in g
};
#endif // EXERCISE_H
