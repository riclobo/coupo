//----------------------------------------------------------------------
// Class definition to generate statitics for an ensemble of exercises
// (c) R.P.S.M. Lobo -- rpsml@yahoo.com
//----------------------------------------------------------------------
#ifndef STATS_H
#define STATS_H
//----------------------------------------------------------------------
#include <iostream>
#include <iomanip>
#include <map>
#include <string>
#include <utility>
#include <vector>
//----------------------------------------------------------------------
#include "fitness.h"
#include "datetime.h"
//----------------------------------------------------------------------
class stats
{
  public:
    // Structure holding cumulative counters
    struct counters
    {
      counters(bool SingleEvent = false) : SglEvnt(SingleEvent)
      {
        if (SglEvnt)
          Events = 1;
        else
          Events = 0;
      }
      counters(const std::string &tag,
               unsigned Kind,
               bool SingleEvent = false) : SglEvnt(SingleEvent), Tag(tag), Exercise(Kind)
      {
        if (SglEvnt)
          Events = 1;
        else
          Events = 0;
      }

      counters &operator=(const counters &rhs);

      bool SglEvnt;

      std::string Tag = "";       // General purpose tag

      unsigned    Events     = 0, // Number of events
                  Exercise   = 0, // Exercise
                  MaxAveHR   = 0, // Maximum heart rate
                  MinAveHR   = 0, // Minimum heart rate
                  TimeActive = 0; // Total time exercising

      double      Distance  = 0.,  // Total distance
                  MaxWeight = 0.,  // Maximum weight
                  MinWeight = 0.,  // Minimum weight
                  Speed     = 0.,  // Average speed
                  Pace      = 0.;  // Average pace
    };

    // Structure holding categories
    struct categ
    {
      categ(bool PrintGlobal = true,
            bool SingleEvent = false) : PrnGlb(PrintGlobal), SglEvnt(SingleEvent) {}

      bool PrnGlb = true,   // When set prints the Global counter in << operator
           SglEvnt = false; // True if container holds a single event

      counters All,
               Run,
               Cycle,
               Swim;

      void CreateCounters()
      {
        All    = counters( "Global",   al::all, SglEvnt);
        Run    = counters( "Running",  al::run, SglEvnt);
        Cycle  = counters( "Cycling",  al::cycle, SglEvnt);
        Swim   = counters( "Swimming", al::swim, SglEvnt);
      }
    };

    // Type definitions
    typedef std::map< std::string, counters >       MapEquip;
    typedef std::pair< std::string, counters >      PairEquip;
    typedef std::vector< std::string >              MapOrder;
    typedef std::map< int, categ >                  MapYear;
    typedef std::pair< int, categ >                 PairYear;
    typedef std::map< std::pair<int,int>, categ >   MapMonth;
    typedef std::pair< std::pair<int,int>, categ >  PairMonth;
    typedef std::map< lobo::YMD, categ >            MapWeek;
    typedef std::pair< lobo::YMD, categ >           PairWeek;

    stats() { }                 // Default constructor
    stats(const fitness &f);    // Constructor with exercises
    ~stats() {}                 // Destructor

    void Add(const exercise &e);                  // Add one exercise to stats
    void Clear();                                 // Clear all records
    void Extremes(const exercise &e);             // Loads fastest and slowest counters
    void Refresh(const fitness &f);               // Recalculates stats
    void ReportEquip(std::ostream &os, size_t ncol) const;  // Writes equipement data to os in n columns
    void SetCounter(counters &c, const exercise &e);        // Set a counter value (sim. to AddTo but no cumulation and puts date in Tag field)
    void SetMax(counters &c, const exercise &e);            // Checks if exercise is fastest
    void SetMin(counters &c, const exercise &e);            // Checks if exercise is slowest

    // References to local counters
    const categ     &Global()  const { return global; }
    const categ     &Fastest() const { return fastest; }
    const categ     &Slowest() const { return slowest; }
    const MapEquip  &Equip()   const { return equip; }
    const MapOrder  &EqOrd()   const { return eqord; }
    const MapYear   &Year()    const { return year; }
    const MapMonth  &Month()   const { return month; }
    const MapWeek   &Week()    const { return week; }

    // Wrappers to map pairs and tuples
    static const std::string &EqName(const PairEquip &p)   { return p.first; }
    static const counters    &EqAct(const PairEquip &p)    { return p.second; }

    static       int          Year(const PairYear &p)      { return p.first; }
    static const categ       &YearCat(const PairYear &p)   { return p.second; }

    static       int          MonthYr(const PairMonth &p)  { return p.first.first; }
    static       int          MonthMo(const PairMonth &p)  { return p.first.second; }
    static const categ       &MonthCat(const PairMonth &p) { return p.second; }

    static const lobo::YMD   &Week(const PairWeek &p)      { return p.first; }
    static       int          WeekYr(const PairWeek &p)    { return std::get<0>(p.first); }
    static       int          WeekMo(const PairWeek &p)    { return std::get<1>(p.first); }
    static       int          WeekDy(const PairWeek &p)    { return std::get<2>(p.first); }
    static const categ       &WeekCat(const PairWeek &p)   { return p.second; }

  private:
    void AddTo(counters &c, const exercise &e);     // Add e to counter
    void AddTo(categ &c, const exercise &e);        // Sorts e into categs
    void CreateEquipments(const fitness &f); // Creates equipement map

    categ     global,           // All time counters
              fastest {false, true},  // Fastest performances
              slowest {false, true};  // Slowest performances

    MapEquip  equip;    // Counters per equipment
    MapOrder  eqord;    // Order the equipment were read
    MapYear   year;     // Counters per year
    MapMonth  month;    // Monthly counters <year, month>
    MapWeek   week;     // Weekly counters (monday)

};
//
// Stream out operators
//
std::ostream &operator<<(std::ostream &os, const stats::counters &c);
std::ostream &operator<<(std::ostream &os, const stats::categ &c);
//----------------------------------------------------------------------
#endif // STATS_H
