//----------------------------------------------------------------------
// Class definition to generate statitics for an ensemble of exercises
// (c) R.P.S.M. Lobo -- rpsml@yahoo.com
//----------------------------------------------------------------------
#include <algorithm>
#include <sstream>
//----------------------------------------------------------------------
#include "stats.h"
#include "datetime.h"
//----------------------------------------------------------------------
// File scoped global variables
//----------------------------------------------------------------------
const int fw = 30;        // Output width for counters
const int nw = 18;        // Name column width in counters output
const int vw = fw - nw;   // Value column width in counters output
//----------------------------------------------------------------------
// Constructor
//----------------------------------------------------------------------
stats::stats(const fitness &f)
{
  Refresh(f);
}
//----------------------------------------------------------------------
// Add one exercise to stats
//----------------------------------------------------------------------
void stats::Add(const exercise &e)
{
  AddTo(global, e);    // Add exercise to global statistics

  // Equipement containers - Already created in CreateEquipments() called from Refresh()
  AddTo(equip[e.GetEquipment()], e);                    // Add exercise to equipement stats

  // Yearly statistics
  int yr = lobo::GetYear( e.GetTimeStamp() ); // Get the year
  if ( year.find(yr) == year.end() )          // Year not in map
    year[yr].CreateCounters();                // Create counters

  AddTo(year[yr], e);                         // Add exercise to year stats

  // Monthly statistics
  // The {yr,mon} syntax is c++11, otherwise use std::make_pair(yr,mon)
  int mon = lobo::GetMonth( e.GetTimeStamp() ); // Get the month
  if ( month.find({yr,mon}) == month.end() )    // Year/Month not in map
    month[{yr,mon}].CreateCounters();           // Create counters

  AddTo(month[{yr,mon}], e);                    // Add exercise to monthly stats

  // Sort by week
  lobo::YMD wk = lobo::WeekBegin( e.GetTimeStamp() ); // Get the week

  if ( week.find(wk) == week.end() )        // Week not in map
    week[wk].CreateCounters();              // Create counters

  AddTo(week[wk], e);                       // Add exercise to weekly stats
}
//----------------------------------------------------------------------
// Loads fastest and slowest counters
//----------------------------------------------------------------------
void stats::Extremes(const exercise &e)
{
  // Checks exercise kind and choses the appropriate container
  switch (e.GetExercise())
  {
    case al::swim:
      SetMin(slowest.Swim, e);
      SetMax(fastest.Swim, e);
      break;

    case al::cycle:
      SetMin(slowest.Cycle, e);
      SetMax(fastest.Cycle, e);
      break;

    case al::run:
    default:
      SetMin(slowest.Run, e);
      SetMax(fastest.Run, e);
      break;
  }
}
//----------------------------------------------------------------------
// Add one exercise to stats
//----------------------------------------------------------------------
void stats::AddTo(stats::counters &c, const exercise &e)
{
  c.Events++;                       // Increase the number of events
  c.TimeActive += e.GetDuration();  // Increase the duration of exercises

  // Distance and speed
  c.Distance   += e.GetDistance();
  c.Speed       = 3600. * c.Distance / double(c.TimeActive);
  c.Pace        = 60. / c.Speed;

  // Check for maximum and minimum average heart rates
  unsigned AveHR = e.GetAveHR();
  if (AveHR != 0)
  {
	c.MaxAveHR = std::max(c.MaxAveHR,AveHR);
	c.MinAveHR = (c.MinAveHR == 0) ? AveHR : std::min(c.MinAveHR,AveHR);
  }

  // Check for maximum and minimum weights
  double Weight = e.GetWeight();
  if (Weight != 0)
  {
    c.MaxWeight = std::max(c.MaxWeight, Weight);
    c.MinWeight = (c.MinWeight < 0.1) ? Weight : std::min(c.MinWeight,Weight);
  }
}
//----------------------------------------------------------------------
// Sorts e into categories
//----------------------------------------------------------------------
void stats::AddTo(categ &c, const exercise &e)
{
 // Add all exercises to global stats
  AddTo(c.All, e);

  // Checks exercise kind and add it to the appropriate container
  switch (e.GetExercise())
  {
    case al::swim:
      AddTo(c.Swim, e);  break;

    case al::cycle:
      AddTo(c.Cycle, e); break;

    case al::run:
    default:
      AddTo(c.Run, e); break;
  }
}
//----------------------------------------------------------------------
// Creates equipement containers with information about their usage
// dates ( Tag = FirstDate|LastDate )
//----------------------------------------------------------------------
void stats::CreateEquipments(const fitness &f)
{
  std::vector<int> tmpExe;
  // Creates list of equipment names in the order they were read
  for (auto & act : f.Exercises())
  {
    std::string eq = act.GetEquipment();
    if ( std::find(eqord.begin(), eqord.end(), eq) == eqord.end() )   // Equipement not in equipemnt list
    {
      eqord.push_back(eq);                       // Equipment name
      tmpExe.push_back( act.GetExercise() );     // Exercise kind
    }
  }

  // Goes trhough the equipemnt list creating entries in the map
  for (size_t i = 0; i < eqord.size(); i++)
  {
    // First and last date the equipment was used
    std::string Tag = lobo::DDMMYYYY( f.FindFirstEquip(eqord[i]) ) + "|"
                    + lobo::DDMMYYYY( f.FindLastEquip(eqord[i]) );

    equip[eqord[i]] = counters(Tag, tmpExe[i]);
  }
}
//----------------------------------------------------------------------
// Clear all records
//----------------------------------------------------------------------
void stats::Clear()
{
  // Resets the 4 global counters
  global.CreateCounters();
  fastest.CreateCounters();
  slowest.CreateCounters();

  // Nothing is slower than stopped
  fastest.Run.Speed = 0.0;
  fastest.Cycle.Speed = 0.0;
  fastest.Swim.Speed = 0.0;

  // Nothing is faster than the light
  slowest.Run.Speed = 3.0E8;
  slowest.Cycle.Speed = 3.0E8;
  slowest.Swim.Speed = 3.0E8;

  equip.clear();            // Clear equipment registers
  year.clear();             // Clear yearly registers
  month.clear();            // Clear monthly registers
  week.clear();             // Clear weekly registers
}
//----------------------------------------------------------------------
// Recalculates stats
//----------------------------------------------------------------------
void stats::Refresh(const fitness &f)
{
  Clear();
  CreateEquipments(f);

  for (auto &activity : f.Exercises())   // Goes through all items...
  {
    Add(activity);                      // ...adds each to the stats...
    Extremes(activity);                 // ...and sets fastest/slowest counters
  }
}
//----------------------------------------------------------------------
// Writes equipement data to os in n columns
//----------------------------------------------------------------------
void stats::ReportEquip(std::ostream &os, size_t ncol) const
{
  std::vector< std::vector<std::string> > fullrep;

  // Creates formatted string list
  for (auto &s : eqord)
  {
    std::stringstream ss;
    std::string aux;
    size_t p;
    std::vector<std::string> rep;
    const counters &ct = equip.at(s);

    ss.str(std::string());
    ss.clear();
    aux = " === " + s + " === ";
    ss << std::left << std::setw(fw) << aux;
    rep.push_back(ss.str());

    p = ct.Tag.find('|');    // Looks for date separator
    ss.str(std::string());
    ss.clear();
    ss << std::left << std::setw(nw) << "First used:" << std::setw(vw) << ct.Tag.substr(0,p);
    rep.push_back(ss.str());

    ss.str(std::string());
    ss.clear();
    ss << std::left << std::setw(nw) << "Last used:" << std::setw(vw) << ct.Tag.substr(p+1);
    rep.push_back(ss.str());

    ss.str(std::string());
    ss.clear();
    ss << std::left << std::setw(nw) << "Exercise:" << std::setw(vw) << exercise::ExeName(ct.Exercise);
    rep.push_back(ss.str());

    ss.str(std::string());
    ss.clear();
    ss << std::left << std::setw(nw) << "# Events:" << std::setw(vw) << ct.Events;
    rep.push_back(ss.str());

    ss.str(std::string());
    ss.clear();
    ss << std::left << std::setw(nw) << "Time:" << std::setw(vw) << lobo::hhmmss(ct.TimeActive);
    rep.push_back(ss.str());

    ss.str(std::string());
    ss.clear();
    ss << std::left << std::setw(nw) << "Distance (km):" << std::setw(vw) << ct.Distance;
    rep.push_back(ss.str());

    ss.str(std::string());
    ss.clear();
    ss << std::left << std::setw(nw) << "Speed (km/h):" << std::setw(vw) << ct.Speed;
    rep.push_back(ss.str());

    ss.str(std::string());
    ss.clear();
    if (ct.Exercise != al::swim)
      ss << std::left << std::setw(nw) << "Pace (min/km):" << std::setw(vw) << lobo::mmmssxxx(ct.Pace);
    else
      ss << std::left << std::setw(nw) << "Pace (min / 100m):" << std::setw(vw) << lobo::mmmssxxx(ct.Pace / 10.);
    rep.push_back(ss.str());

    ss.str(std::string());
    ss.clear();
    ss << std::left << std::setw(nw) << "Max. Average HR:" << std::setw(vw) << ct.MaxAveHR;
    rep.push_back(ss.str());

    ss.str(std::string());
    ss.clear();
    ss << std::left << std::setw(nw) << "Min. Average HR:" << std::setw(vw) << ct.MinAveHR;
    rep.push_back(ss.str());

    ss.str(std::string());
    ss.clear();
    ss << std::left << std::setw(nw) << "Max. Weight (kg):" << std::setw(vw) << ct.MaxWeight;
    rep.push_back(ss.str());

    ss.str(std::string());
    ss.clear();
    ss << std::left << std::setw(nw) << "Min. Weight (kg):" << std::setw(vw) << ct.MinWeight;
    rep.push_back(ss.str());

    fullrep.push_back(rep);
  }

  // Saves report in ensembles of ncol columns
  size_t nlines{13},    // Number of items in each report
         eq_i{0};

  while (eq_i < eqord.size())
  {
    for (size_t line = 0; line < nlines; line++)
    {
      size_t m = eq_i + ncol;
      if (m > eqord.size()) m = eqord.size();

      for (size_t n = eq_i; n < m; n++)
        os << fullrep[n][line] << ((n + 1) % ncol ? "| " : "");

      os << std::endl;
    }
    eq_i += ncol;
    os << std::endl;
    os << std::endl;
  }
}
//----------------------------------------------------------------------
// Set a counter value. Similar to AddTo but no cumulation and puts date
// in Tag field.
//----------------------------------------------------------------------
void stats::SetCounter(counters &c, const exercise &e)
{
  c.Tag = lobo::DDMMYYYY( e.GetTimeStamp() );   // Date in tag
  c.Events = 1;                                 // One event
  c.TimeActive = e.GetDuration();               // Duration of exercise

  // Distance and speed
  c.Distance    = e.GetDistance();
  c.Speed       = 3600. * c.Distance / double(c.TimeActive);
  c.Pace        = 60. / c.Speed;

  // Heart rate and weight
  c.MaxAveHR  = c.MinAveHR  = e.GetAveHR();
  c.MaxWeight = c.MinWeight = e.GetWeight();
}
//----------------------------------------------------------------------
// Checks if exercise is fastest
//----------------------------------------------------------------------
void stats::SetMax(counters &c, const exercise &e)
{
  if (e.Speed() > c.Speed)
    SetCounter(c,e);
}
//----------------------------------------------------------------------
// Checks if exercise is slowest
//----------------------------------------------------------------------
void stats::SetMin(counters &c, const exercise &e)
{
  if (e.Speed() < c.Speed)
    SetCounter(c,e);
}
//----------------------------------------------------------------------
// = operator for counters struct
//----------------------------------------------------------------------
stats::counters &stats::counters::operator=(const stats::counters &rhs)
{
  if (this != &rhs)
  {
    SglEvnt     = rhs.SglEvnt;
    Tag         = rhs.Tag;
    Exercise    = rhs.Exercise;
    Events      = rhs.Events;
    TimeActive  = rhs.TimeActive;
    MaxAveHR    = rhs.MaxAveHR;
    MinAveHR    = rhs.MinAveHR;
    Distance    = rhs.Distance;
    MaxWeight   = rhs.MaxWeight;
    MinWeight   = rhs.MinWeight;
    Speed       = rhs.Speed;
    Pace        = rhs.Pace;
  }
  return *this;
}
//----------------------------------------------------------------------
// << friend operator for counters struct
//----------------------------------------------------------------------
std::ostream &operator<<(std::ostream &os, const stats::counters &c)
{
  os << "Tag:             " << c.Tag                          << std::endl
     << "Exercise:        " << exercise::ExeName(c.Exercise)  << std::endl
     << "# Events:        " << c.Events                       << std::endl
     << "Time:            " << lobo::hhmmss(c.TimeActive)     << std::endl
     << "Distance:        " << c.Distance << " km"            << std::endl
     << "Speed:           " << c.Speed    << " km/h"          << std::endl;

  if (c.Tag.find("Swim") == std::string::npos)    // This is probably not a swim only activity
    os << "Pace:            "  << lobo::mmmssxxx(c.Pace) << " min/km" << std::endl;
  else
    os << "Pace:            "  << lobo::mmmssxxx(c.Pace / 10.) << " min / 100m" << std::endl;

  if (!c.SglEvnt)
  {
    os << "Max. Average HR: " << c.MaxAveHR                 << std::endl
       << "Min. Average HR: " << c.MinAveHR                 << std::endl
       << "Max. Weight:     " << c.MaxWeight                << std::endl
       << "Min. Weight:     " << c.MinWeight                << std::endl;
  }
  else
  {
    os << "Heart Rate: "      << c.MaxAveHR                 << std::endl
       << "Weight:     "      << c.MaxWeight                << std::endl;
  }

  return os;
}
//----------------------------------------------------------------------
// << friend operator for categ struct
// fw, nw, and vw defined at the top ot this file
//----------------------------------------------------------------------
std::ostream &operator<<(std::ostream &os, const stats::categ &c)
{
  os << std::left;

  if (c.PrnGlb) os << std::setw(fw) << "Global"    << "| ";

  os << std::setw(fw) << "Running"   << "| "
     << std::setw(fw) << "Cycling"   << "| "
     << std::setw(fw) << "Swimming"  << std::endl;

  if (c.PrnGlb) os << std::setw(nw) << "Tag:" << std::setw(vw) << c.All.Tag  << "| ";

  os << std::setw(nw) << "Tag:" << std::setw(vw) << c.Run.Tag     << "| "
     << std::setw(nw) << "Tag:" << std::setw(vw) << c.Cycle.Tag   << "| "
     << std::setw(nw) << "Tag:" << std::setw(vw) << c.Swim.Tag    << std::endl;

  if (c.PrnGlb) os << std::setw(nw) << "Exercise:" << std::setw(vw) << exercise::ExeName(c.All.Exercise)  << "| ";

  os << std::setw(nw) << "Exercise:" << std::setw(vw) << exercise::ExeName(c.Run.Exercise)     << "| "
     << std::setw(nw) << "Exercise:" << std::setw(vw) << exercise::ExeName(c.Cycle.Exercise)   << "| "
     << std::setw(nw) << "Exercise:" << std::setw(vw) << exercise::ExeName(c.Swim.Exercise)    << std::endl;

  if (c.PrnGlb) os << std::setw(nw) << "# Events:" << std::setw(vw) << c.All.Events  << "| ";

  os << std::setw(nw) << "# Events:" << std::setw(vw) << c.Run.Events     << "| "
     << std::setw(nw) << "# Events:" << std::setw(vw) << c.Cycle.Events   << "| "
     << std::setw(nw) << "# Events:" << std::setw(vw) << c.Swim.Events    << std::endl;

  if (c.PrnGlb) os << std::setw(nw) << "Time:" << std::setw(vw) << lobo::hhmmss(c.All.TimeActive)  << "| ";

  os << std::setw(nw) << "Time:" << std::setw(vw) << lobo::hhmmss(c.Run.TimeActive)     << "| "
     << std::setw(nw) << "Time:" << std::setw(vw) << lobo::hhmmss(c.Cycle.TimeActive)   << "| "
     << std::setw(nw) << "Time:" << std::setw(vw) << lobo::hhmmss(c.Swim.TimeActive)    << std::endl;

  if (c.PrnGlb) os << std::setw(nw) << "Distance (km):" << std::setw(vw) << c.All.Distance  << "| ";

  os << std::setw(nw) << "Distance (km):" << std::setw(vw) << c.Run.Distance     << "| "
     << std::setw(nw) << "Distance (km):" << std::setw(vw) << c.Cycle.Distance   << "| "
     << std::setw(nw) << "Distance (km):" << std::setw(vw) << c.Swim.Distance    << std::endl;

  if (c.PrnGlb) os << std::setw(nw) << "Speed (km/h):" << std::setw(vw) << c.All.Speed  << "| ";

  os << std::setw(nw) << "Speed (km/h):" << std::setw(vw) << c.Run.Speed     << "| "
     << std::setw(nw) << "Speed (km/h):" << std::setw(vw) << c.Cycle.Speed   << "| "
     << std::setw(nw) << "Speed (km/h):" << std::setw(vw) << c.Swim.Speed    << std::endl;

  if (c.PrnGlb) os << std::setw(nw) << "Pace (min/km):"     << std::setw(vw) << lobo::mmmssxxx(c.All.Pace)     << "| ";

  os << std::setw(nw) << "Pace (min/km):"     << std::setw(vw) << lobo::mmmssxxx(c.Run.Pace)        << "| "
     << std::setw(nw) << "Pace (min/km):"     << std::setw(vw) << lobo::mmmssxxx(c.Cycle.Pace)      << "| "
     << std::setw(nw) << "Pace (min / 100m):" << std::setw(vw) << lobo::mmmssxxx(c.Swim.Pace / 10.) << std::endl;


  if (!c.SglEvnt)
  {
    if (c.PrnGlb) os << std::setw(nw) << "Max. Average HR:" << std::setw(vw) << c.All.MaxAveHR  << "| ";

    os << std::setw(nw) << "Max. Average HR:" << std::setw(vw) << c.Run.MaxAveHR     << "| "
       << std::setw(nw) << "Max. Average HR:" << std::setw(vw) << c.Cycle.MaxAveHR   << "| "
       << std::setw(nw) << "Max. Average HR:" << std::setw(vw) << c.Swim.MaxAveHR    << std::endl;

    if (c.PrnGlb) os << std::setw(nw) << "Min. Average HR:" << std::setw(vw) << c.All.MinAveHR  << "| ";

    os << std::setw(nw) << "Min. Average HR:" << std::setw(vw) << c.Run.MinAveHR     << "| "
       << std::setw(nw) << "Min. Average HR:" << std::setw(vw) << c.Cycle.MinAveHR   << "| "
       << std::setw(nw) << "Min. Average HR:" << std::setw(vw) << c.Swim.MinAveHR    << std::endl;

    if (c.PrnGlb) os << std::setw(nw) << "Max. Weight (kg):" << std::setw(vw) << c.All.MaxWeight  << "| ";

    os << std::setw(nw) << "Max. Weight (kg):" << std::setw(vw) << c.Run.MaxWeight     << "| "
       << std::setw(nw) << "Max. Weight (kg):" << std::setw(vw) << c.Cycle.MaxWeight   << "| "
       << std::setw(nw) << "Max. Weight (kg):" << std::setw(vw) << c.Swim.MaxWeight    << std::endl;

    if (c.PrnGlb) os << std::setw(nw) << "Min. Weight (kg):" << std::setw(vw) << c.All.MinWeight  << "| ";

    os << std::setw(nw) << "Min. Weight (kg):" << std::setw(vw) << c.Run.MinWeight     << "| "
       << std::setw(nw) << "Min. Weight (kg):" << std::setw(vw) << c.Cycle.MinWeight   << "| "
       << std::setw(nw) << "Min. Weight (kg):" << std::setw(vw) << c.Swim.MinWeight    << std::endl;
  }
  else
  {
    if (c.PrnGlb) os << std::setw(nw) << "Heart Rate:" << std::setw(vw) << c.All.MaxAveHR  << "| ";

    os << std::setw(nw) << "Heart Rate:" << std::setw(vw) << c.Run.MaxAveHR     << "| "
       << std::setw(nw) << "Heart Rate:" << std::setw(vw) << c.Cycle.MaxAveHR   << "| "
       << std::setw(nw) << "Heart Rate:" << std::setw(vw) << c.Swim.MaxAveHR    << std::endl;

    if (c.PrnGlb) os << std::setw(nw) << "Weight (kg):" << std::setw(vw) << c.All.MaxWeight  << "| ";

    os << std::setw(nw) << "Weight (kg):" << std::setw(vw) << c.Run.MaxWeight     << "| "
       << std::setw(nw) << "Weight (kg):" << std::setw(vw) << c.Cycle.MaxWeight   << "| "
       << std::setw(nw) << "Weight (kg):" << std::setw(vw) << c.Swim.MaxWeight    << std::endl;
  }

  return os;
}
